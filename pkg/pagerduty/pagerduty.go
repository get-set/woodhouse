package pagerduty

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"github.com/rs/xid"
	"github.com/sethvargo/go-retry"
)

type Pager struct {
	HTTPClient      *http.Client
	PagerdutyClient *gopagerduty.Client
}

// TODO replace HTTP client code with go-pagerduty client code. Apparently that
// library does support the Events V2 API. Initially, I mistakenly thought it
// didn't.
func (p *Pager) Page(ctx context.Context, integrationKey, message string, slackURL string) error {
	if integrationKey == "" {
		return errors.New("Integration key not set for this service")
	}

	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	dedupKey := xid.New().String()

	body, err := json.Marshal(PagerdutyEvent{
		RoutingKey:  integrationKey,
		DedupKey:    dedupKey,
		EventAction: "trigger",
		Payload: PagerdutyEventPayload{
			Summary:   message,
			Source:    hostname,
			Severity:  "info",
			Component: "woodhouse",
			CustomDetails: PagerdutyCustomDetails{
				Type: "woodhouse",
			},
		},
		Links: []PagerdutyLink{
			{Text: "Slack channel", Href: slackURL},
		},
	})
	if err != nil {
		return err
	}

	return retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		resp, err := p.HTTPClient.Post("https://events.pagerduty.com/v2/enqueue", "application/json", bytes.NewReader(body))
		if err != nil {
			return retry.RetryableError(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 202 {
			return retry.RetryableError(fmt.Errorf("expected HTTP status 202, got %d", resp.StatusCode))
		}
		return nil
	})
}

func (p *Pager) GetOncallEmails(ctx context.Context, escalationPolicies []string, since, until *time.Time) ([]string, error) {
	var onCallEmails []string
	policies := removeEmpty(escalationPolicies)
	if len(policies) == 0 {
		return nil, nil
	}
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		sinceStr := ""
		if since != nil {
			sinceStr = since.Format(time.RFC3339)
		}
		untilStr := ""
		if until != nil {
			untilStr = until.Format(time.RFC3339)
		}

		onCalls, err := p.PagerdutyClient.ListOnCalls(gopagerduty.ListOnCallOptions{
			EscalationPolicyIDs: policies,
			Since:               sinceStr,
			Until:               untilStr,
			Earliest:            true,
			Includes:            []string{"users"},
		})
		if err != nil {
			return retry.RetryableError(err)
		}
		if onCalls.More {
			return fmt.Errorf("expected only 1 page of results for escalation policies: %s", strings.Join(policies, ", "))
		}
		for _, onCall := range onCalls.OnCalls {
			// Only take the first on-call
			if onCall.EscalationLevel != 1 {
				continue
			}
			if onCall.User.Email == "" {
				continue
			}
			onCallEmails = append(onCallEmails, onCall.User.Email)
		}
		return nil
	})
	return onCallEmails, err
}

func (p *Pager) GetOncallShiftStartAndEndTime(ctx context.Context, escalationPolicies []string, since, until *time.Time) (string, string, error) {
	policies := removeEmpty(escalationPolicies)
	if len(policies) == 0 {
		return "", "", nil
	}
	sinceStr := ""
	if since != nil {
		sinceStr = since.Format(time.RFC3339)
	}
	untilStr := ""
	if until != nil {
		untilStr = until.Format(time.RFC3339)
	}

	onCalls, err := p.PagerdutyClient.ListOnCalls(gopagerduty.ListOnCallOptions{
		EscalationPolicyIDs: policies,
		Since:               sinceStr,
		Until:               untilStr,
		Earliest:            true,
	})
	if err != nil {
		return "", "", err
	}
	if onCalls.More {
		return "", "", fmt.Errorf("expected only 1 page of results for escalation policies: %s", strings.Join(policies, ", "))
	}
	if len(onCalls.OnCalls) == 0 {
		return "", "", fmt.Errorf("no on-calls found for escalation policies: %s", strings.Join(policies, ", "))
	}

	for _, onCall := range onCalls.OnCalls {
		if onCall.EscalationLevel != 1 {
			continue
		}
		return onCall.Start, onCall.End, nil
	}

	return "", "", fmt.Errorf("no first-level on-calls found for escalation policies: %s", strings.Join(policies, ", "))
}

type PagerdutyEvent struct {
	RoutingKey  string                `json:"routing_key"`
	DedupKey    string                `json:"dedup_key"`
	EventAction string                `json:"event_action"`
	Payload     PagerdutyEventPayload `json:"payload"`
	Links       []PagerdutyLink       `json:"links"`
}

type PagerdutyEventPayload struct {
	Summary       string                 `json:"summary"`
	Source        string                 `json:"source"`
	Severity      string                 `json:"severity"`
	Component     string                 `json:"component"`
	CustomDetails PagerdutyCustomDetails `json:"custom_details"`
}

type PagerdutyCustomDetails struct {
	Type string `json:"type"`
}

type PagerdutyLink struct {
	Href string `json:"href"`
	Text string `json:"text"`
}

func removeEmpty(arr []string) []string {
	var removedEmpties []string
	for _, s := range arr {
		if s != "" {
			removedEmpties = append(removedEmpties, s)
		}
	}
	return removedEmpties
}

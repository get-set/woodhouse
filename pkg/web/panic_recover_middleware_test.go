package web_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/web"
)

func TestPanicRecoverMiddleware_ItCallsTheNextHandler(t *testing.T) {
	server := setupPanicRecoverMiddlewareTest(t, func(interface{}, *http.Request) {
		t.Error("did not expect panic handler to be called")
	})
	defer server.Close()

	resp, err := server.Client().Get(server.URL + "/dontpanic")
	require.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, 202, resp.StatusCode)
}

func TestPanicRecoverMiddleware_ItReturnsHTTP500AndCallsPanicHandlerOnPanic(t *testing.T) {
	handlerCalled := false
	server := setupPanicRecoverMiddlewareTest(t, func(panicked interface{}, _ *http.Request) {
		assert.Equal(t, "panic!", panicked)
		handlerCalled = true
	})
	defer server.Close()

	resp, err := server.Client().Get(server.URL + "/panic")
	require.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, 500, resp.StatusCode)

	assert.True(t, handlerCalled)
}

func setupPanicRecoverMiddlewareTest(t *testing.T, onPanic func(interface{}, *http.Request)) *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/dontpanic", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusAccepted)
	})
	handler.HandleFunc("/panic", func(w http.ResponseWriter, r *http.Request) {
		panic("panic!")
	})
	middleware := web.NewPanicRecoverMiddleware(onPanic, handler)

	return httptest.NewServer(middleware)
}

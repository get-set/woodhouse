package slackapps

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type InteractivityHandler struct {
	handlers map[string]InteractionHandler
}

type InteractionHandler interface {
	Handle(
		event *instrumentation.Event, command slack.InteractionCallback,
		w http.ResponseWriter, req *http.Request,
	)
}

func NewInteractivityHandler() *InteractivityHandler {
	return &InteractivityHandler{handlers: map[string]InteractionHandler{}}
}

func (i *InteractivityHandler) HandleInteraction(callbackID string, handler InteractionHandler) {
	i.handlers[callbackID] = handler
}

func (i *InteractivityHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

	var payload slack.InteractionCallback
	err := json.Unmarshal([]byte(req.FormValue("payload")), &payload)
	if err != nil {
		event.With("error", err, "message", "error parsing interaction callback")
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	handler := i.handlers[payload.View.CallbackID]
	if handler == nil {
		errMsg := "no interaction handler found for callback ID " + payload.CallbackID
		event.With("error", errMsg)
		fmt.Fprintln(w, errMsg)
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	handler.Handle(event, payload, w, req)
}

package slackapps

import (
	"fmt"
	"time"

	"github.com/iancoleman/strcase"
	"github.com/slack-go/slack"
)

func PlainText(text string) *slack.TextBlockObject {
	return slack.NewTextBlockObject("plain_text", text, false, false)
}

func TextInputBlock(title, placeholder string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	input := slack.NewPlainTextInputBlockElement(PlainText(placeholder), id)
	return slack.NewInputBlock(id, PlainText(title), input)
}

func SelectInputBlock(title, placeholder string, initialOptionIndex int, options ...string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	var selectOpts []*slack.OptionBlockObject
	for _, option := range options {
		selectOpts = append(selectOpts, slack.NewOptionBlockObject(option, PlainText(option), nil))
	}
	input := slack.NewOptionsSelectBlockElement("static_select", PlainText(placeholder), id, selectOpts...)
	if initialOptionIndex >= 0 && initialOptionIndex < len(selectOpts) {
		input.InitialOption = selectOpts[initialOptionIndex]
	}
	return slack.NewInputBlock(id, PlainText(title), input)
}

func ChannelLink(channelID, teamID string) string {
	return fmt.Sprintf("https://slack.com/app_redirect?channel=%s&team=%s", channelID, teamID)
}

func MultiSelectBlock(title string, placeholder string, options ...string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)

	var selectOpts []*slack.OptionBlockObject
	for _, option := range options {
		selectOpts = append(selectOpts, slack.NewOptionBlockObject(option, PlainText(option), nil))
	}

	input := slack.NewOptionsMultiSelectBlockElement("multi_static_select", PlainText(placeholder), id, selectOpts...)
	return slack.NewInputBlock(id, PlainText(title), input)
}

func DatePicker(title string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	input := slack.NewDatePickerBlockElement("date")
	input.InitialDate = time.Now().UTC().Format("2006-01-02")

	return slack.NewInputBlock(id, PlainText(title), input)
}

func SingleCheckboxBlock(title string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	input :=
		slack.NewCheckboxGroupsBlockElement(
			id,
			slack.NewOptionBlockObject("downtime", PlainText("This change will require downtime"), nil),
		)
	return slack.NewInputBlock(id, PlainText(title), input)
}

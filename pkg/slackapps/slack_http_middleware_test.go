package slackapps_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
)

// Including a "success" test case is too fiddly to be worth doing, as the
// slack-go library's verification depends on the current time, which can't be
// injected. Ensuring that handler's aren't called when garbage is passed in is
// good enough.
func TestSlackHTTPMiddleware(t *testing.T) {
	for _, tc := range []struct {
		name           string
		reqHeaders     http.Header
		expectedStatus int
	}{
		{
			name:           "Returns HTTP 422 when no headers specified",
			expectedStatus: 422,
		},
		{
			name: "Returns HTTP 403 when headers specified but with a bogus signature",
			reqHeaders: http.Header{
				"X-Slack-Signature":         []string{"v0=a2114d57b48eac39b9ad189dd8316235a7b4a8d21a10bd27519666489c69b503"},
				"X-Slack-Request-Timestamp": []string{oneMinuteAgo()},
			},
			expectedStatus: 403,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
				t.Error("did not expect handler to be called")
			})
			server := httptest.NewServer(instrumentation.NewDummyInstrumentationMiddleware(
				slackapps.NewSlackHTTPMiddleware("it's a secret", handler),
			))
			defer server.Close()

			req, err := http.NewRequest(http.MethodGet, server.URL, nil)
			require.Nil(t, err)
			req.Header = tc.reqHeaders
			resp, err := server.Client().Do(req)
			require.Nil(t, err)
			defer resp.Body.Close()
			assert.Equal(t, tc.expectedStatus, resp.StatusCode)
		})
	}
}

func oneMinuteAgo() string {
	return fmt.Sprintf("%d", time.Now().Add(-time.Minute).UTC().Unix())
}

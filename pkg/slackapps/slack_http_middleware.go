package slackapps

import (
	"bytes"
	"fmt"
	"io"
	"net/http"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

func NewSlackHTTPMiddleware(signingSecret string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

		verifier, err := slack.NewSecretsVerifier(req.Header, signingSecret)
		if err != nil {
			event.With("error", err, "message", "error creating slack.NewSecretsVerifier")
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprintln(w, err)
			return
		}

		var copiedBody bytes.Buffer
		teeBody := io.TeeReader(req.Body, &copiedBody)
		if _, err := io.Copy(&verifier, teeBody); err != nil {
			event.With("error", err, "message", "error reading request body")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if err := verifier.Ensure(); err != nil {
			event.With("error", err, "message", "error verifying Slack headers")
			w.WriteHeader(http.StatusForbidden)
			return
		}

		req.Body.Close()
		req.Body = io.NopCloser(&copiedBody)

		next.ServeHTTP(w, req)
	})
}

package slackapps

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type EchoSlashCommand struct{}

func (s *EchoSlashCommand) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeInChannel,
		Text:         fmt.Sprintf("<@%s> in <#%s> says: %s", command.UserID, command.ChannelID, command.Text),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}

package incidents_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/incidents"
)

func TestIssueUpdateMessage(t *testing.T) {
	for _, tc := range []struct {
		name              string
		issueEvent        *gitlab.IssueEvent
		shouldPostMessage bool
	}{
		{
			name:              "when an incident issue is opened, posts a message",
			issueEvent:        newIncident(),
			shouldPostMessage: true,
		},
		{
			name:              "when a non-incident issue is opened, does not post a message",
			issueEvent:        newNonIncident(),
			shouldPostMessage: false,
		},
		{
			name:              "when an incident issue is opened by woodhouse, does not post a message",
			issueEvent:        newWoodhouseIncident(),
			shouldPostMessage: false,
		},
		{
			name:              "when an incident issue is reopened, posts a message",
			issueEvent:        reopenedIncident(),
			shouldPostMessage: true,
		},
		{
			name:              "when a non-incident issue is reopened, does not post a message",
			issueEvent:        reopenedNonIncident(),
			shouldPostMessage: false,
		},
		{
			name:              "when an incident issue that was opened by woodhouse is reopened, posts a message",
			issueEvent:        reopenedWoodhouseIncident(),
			shouldPostMessage: true,
		},
		{
			name:              "when an incident issue is relabeled to active, posts a message",
			issueEvent:        newRelabeledActiveIncident(),
			shouldPostMessage: true,
		},
		{
			name:              "when an incident issue is relabeled but not to active active, does not post a message",
			issueEvent:        newRelabeledInactiveIncident(),
			shouldPostMessage: false,
		},
		{
			name:              "when an incident issue's severity is changed, posts a message",
			issueEvent:        newRelabeledSeverityIncident(),
			shouldPostMessage: true,
		},
		{
			name:              "when an active incident's non-severity label is changed, does not post a message",
			issueEvent:        newRelabeledButNotInterestingIncident(),
			shouldPostMessage: false,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.shouldPostMessage, incidents.IssueHasNotableUpdate(tc.issueEvent))
		})
	}
}

func newIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "open"
	event.Labels = append(event.Labels, label("incident"))
	return event
}

func newNonIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "open"
	event.Labels = append(event.Labels, label("whatever"))
	return event
}

func newWoodhouseIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "open"
	event.Labels = append(event.Labels, label("incident"), label("Source::IMA::IncidentDeclare"))
	return event
}

func reopenedIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "reopen"
	event.Labels = append(event.Labels, label("incident"))
	return event
}

func reopenedNonIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "reopen"
	event.Labels = append(event.Labels, label("whatever"))
	return event
}

func reopenedWoodhouseIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "reopen"
	event.Labels = append(event.Labels, label("incident"), label("Source::IMA::IncidentDeclare"))
	return event
}

func newRelabeledActiveIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "update"
	event.Labels = []gitlab.Label{label(incidents.ActiveIncidentLabel), label("incident")}
	event.Changes.Labels.Current = []gitlab.Label{label(incidents.ActiveIncidentLabel), label("incident")}
	return event
}

func newRelabeledInactiveIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "update"
	event.Changes.Labels.Current = []gitlab.Label{label("Incident::Mitigated"), label("incident")}
	return event
}

func newRelabeledSeverityIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "update"
	event.Labels = []gitlab.Label{label("incident"), label("severity::1")}
	event.Changes.Labels.Current = []gitlab.Label{label("incident"), label("severity::1")}
	event.Changes.Labels.Previous = []gitlab.Label{label("incident"), label("severity::2")}
	return event
}

func newRelabeledButNotInterestingIncident() *gitlab.IssueEvent {
	event := &gitlab.IssueEvent{}
	event.ObjectAttributes.Action = "update"
	event.Labels = []gitlab.Label{label("incident"), label("severity::1"), label("foo")}
	event.Changes.Labels.Current = []gitlab.Label{label("incident"), label("severity::1"), label("foo")}
	event.Changes.Labels.Previous = []gitlab.Label{label("incident"), label("severity::1")}
	return event
}

func label(name string) gitlab.Label {
	return gitlab.Label{Name: name}
}

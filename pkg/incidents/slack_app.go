package incidents

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
	"text/template"
	"time"

	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

const (
	ModalCallbackID = "incident-declare"

	// Channel topics and purposes both have a documented max length of 250
	// characters. Accomodating a realistic URL for the markdown link and the
	// wrapper text, we could get away with a slightly higher limit, but let's
	// play it safe.
	issueTitleMaxLengthForSlack = 85
	spinTheWheelNumDays         = 7
	spinTheWheelSeverity        = "severity::2"
)

var (
	whitespace = regexp.MustCompile(`\s+`)
)

type SlashCommand struct {
	SlackClient                 *slack.Client
	JobRunner                   *woodhouse.AsyncJobRunner
	IncidentChannelID           string
	GitlabProductionProjectPath string
	GitlabClient                *gitlab.Client
}

type IncidentContext struct {
	UserID            string
	Username          string
	AnnounceChannelID string
	TeamID            string
}

func (s *SlashCommand) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	subcommandArgs := whitespace.Split(command.Text, -1)
	if len(subcommandArgs) != 1 {
		s.help(event, commandName, command, w, req)
		return
	}

	switch subcommandArgs[0] {
	case "declare":
		s.declare(event, command, w, req)
	case "spin-the-wheel":
		s.spinTheWheel(event, command, w, req)
	default:
		s.help(event, commandName, command, w, req)
	}
}

func (s *SlashCommand) spinTheWheel(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	issueInterval := time.Now().Add(-24 * spinTheWheelNumDays * time.Hour)
	issues, _, err := s.GitlabClient.Issues.ListProjectIssues(
		s.GitlabProductionProjectPath, &gitlab.ListProjectIssuesOptions{
			Labels:       []string{"Incident::Resolved", spinTheWheelSeverity},
			CreatedAfter: &issueInterval,
		},
	)
	if err != nil {
		event.With("error", err, "message", "error fetching issues")
		return
	}

	if len(issues) == 0 {
		if err := json.NewEncoder(w).Encode(slack.Msg{
			Text:         ":wheel_of_dharma: No issues available for spin-the-wheel :yay:",
			ResponseType: slack.ResponseTypeInChannel,
		}); err != nil {
			event.With("error", err, "message", "error responding to slash command")
			return
		}
		return
	}

	rand.Seed(time.Now().Unix())
	randIssue := issues[rand.Intn(len(issues))]

	if err := json.NewEncoder(w).Encode(slack.Msg{
		Text:         fmt.Sprintf(":wheel_of_dharma: Picking random sev2 incident %s for spin-the-wheel!", randIssue.WebURL),
		ResponseType: slack.ResponseTypeInChannel,
	}); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}
}

func (s *SlashCommand) declare(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	// Do not alarm the on-call by echoing back the slash command, or posting any
	// messages visible by anyone other than the caller. These would be
	// unactionable until the modal form has been submitted.
	msg := slack.Msg{
		Text:         fmt.Sprintf("I'm on it! Please fill in the form, which will declare an incident in <#%s>.", s.IncidentChannelID),
		ResponseType: slack.ResponseTypeEphemeral,
	}
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}

	s.JobRunner.RunAsyncJob("present_incident_modal", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return s.presentIncidentModal(ctx, event, &command)
	})
}

func (s *SlashCommand) help(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeEphemeral,
		Text:         fmt.Sprintf("Usage: %s declare", commandName),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}

func (s *SlashCommand) presentIncidentModal(ctx context.Context, event *instrumentation.Event, command *slack.SlashCommand) error {
	callbackData, err := json.Marshal(IncidentContext{
		UserID:            command.UserID,
		Username:          command.UserName,
		AnnounceChannelID: s.IncidentChannelID,
		TeamID:            command.TeamID,
	})
	if err != nil {
		return err
	}

	// Gather all errors and return them at the end, to do as much useful work as
	// possible for the modal dialog
	var errs []error

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slackapps.PlainText("Declare an incident"),
		Close:           slackapps.PlainText("Close"),
		Submit:          slackapps.PlainText("Submit"),
		CallbackID:      ModalCallbackID,
		PrivateMetadata: string(callbackData),
	}

	titlePlaceholder := time.Now().UTC().Format("2006-01-02") + ": Errors reticulating splines..."
	titleBlock := slackapps.TextInputBlock("Title", titlePlaceholder)
	severityHelpLink := slack.NewTextBlockObject("mrkdwn", "<https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability|Handbook: when to use each severity label>", false, false)
	severityHelpBlock := slack.NewSectionBlock(severityHelpLink, nil, nil)
	severityBlock := slackapps.SelectInputBlock(
		"Severity", "Choose a severity",
		2,
		Severity(1).IssueLabel(),
		Severity(2).IssueLabel(),
		Severity(3).IssueLabel(),
		Severity(4).IssueLabel(),
	)
	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, titleBlock, severityHelpBlock, severityBlock)

	serviceLabelNames, err := gitlabutil.GetServiceLabels(s.GitlabClient, s.GitlabProductionProjectPath)

	if err != nil {
		errs = append(errs, err)
		timeoutMsg := slack.NewTextBlockObject("mrkdwn", "🚨 Timeout connecting to GitLab.com, issue creation will likely fail 🚨", false, false)
		timeoutMsgBlock := slack.NewSectionBlock(timeoutMsg, nil, nil)
		modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, timeoutMsgBlock)
	} else {
		servicesBlock := slackapps.SelectInputBlock("Service", "Choose service", -1,
			serviceLabelNames...)
		servicesBlock.Optional = true
		modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, servicesBlock)
	}

	pageEOC := slack.NewOptionBlockObject("pageEOC", slackapps.PlainText("Page engineer on-call"), nil)
	pageIM := slack.NewOptionBlockObject("pageIM", slackapps.PlainText("Page incident manager"), nil)
	pageCMOC := slack.NewOptionBlockObject("pageCMOC", slackapps.PlainText("Page communications manager on-call"), nil)

	checkboxGroupsBlock := slack.NewCheckboxGroupsBlockElement(
		"tasks",
		pageEOC,
		pageIM,
		pageCMOC,
	)
	checkboxGroupsBlock.InitialOptions = append(checkboxGroupsBlock.InitialOptions, pageEOC, pageIM, pageCMOC)

	tasksBlock := slack.NewInputBlock(
		"tasks", slackapps.PlainText("Page for severity::1 and severity::2"),
		checkboxGroupsBlock,
	)
	tasksBlock.Optional = true

	extraLabelsBlock := slack.NewInputBlock(
		"extralabels", slackapps.PlainText("Incident Issue Labels"),
		slack.NewCheckboxGroupsBlockElement(
			"extralabels",
			slack.NewOptionBlockObject("incident-type::deployment related", slackapps.PlainText("incident-type::deployment related"), nil),
		),
	)
	extraLabelsBlock.Optional = true

	confidentialBlock := slack.NewInputBlock(
		"confidential", slackapps.PlainText("Confidential"),
		slack.NewCheckboxGroupsBlockElement(
			"confidential",
			slack.NewOptionBlockObject("confidential", slackapps.PlainText("Mark issue as confidential"), nil),
		),
	)
	confidentialBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, tasksBlock, confidentialBlock, extraLabelsBlock)

	_, err = s.SlackClient.OpenView(command.TriggerID, modal)
	if err != nil {
		errs = append(errs, err)
	}

	return woodhouse.MultiError(errs)
}

type IncidentModalHandler struct {
	JobRunner                   *woodhouse.AsyncJobRunner
	SlackClient                 *slack.Client
	GitlabClient                *gitlab.Client
	GitlabGraphQLClient         *gitlabutil.GraphQLClient
	GitlabProductionProjectPath string
	GitLabProfileSlackFieldID   string
	GitLabIssueType             string

	GitlabOpsClient            *gitlab.Client
	GitLabOpsCheckProject      string
	GitlabOpsCheckTriggerToken string

	IncidentChannelNamePrefix string
	IncidentCallURL           string

	Pager                         *pagerduty.Pager
	PagerdutyIntegrationKeyEOC    string
	PagerdutyIntegrationKeyIMOC   string
	PagerdutyIntegrationKeyCMOC   string
	PagerdutyEscalationPolicyEOC  string
	PagerdutyEscalationPolicyIMOC string
}

func (i *IncidentModalHandler) Handle(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) {
	i.JobRunner.RunAsyncJob("create_incident", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return i.createIncident(ctx, event, &payload)
	})
}

// We want to do as much useful work as possible if the GitLab incident issue
// creation fails. This isn't very unrealistic: after all, we're only calling
// this in response to a GitLab incident! If the GitLab incident issue creation
// fails, we still create a Slack channel, and send pages (if requested).
func (i *IncidentModalHandler) createIncident(ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback) error {
	event.With("job", "create_incident")

	var incident IncidentContext
	if err := json.Unmarshal([]byte(payload.View.PrivateMetadata), &incident); err != nil {
		return err
	}
	title := payload.View.State.Values["title"]["title"].Value
	severityLabel := payload.View.State.Values["severity"]["severity"].SelectedOption.Value
	severity, err := ParseSeverityFromIssueLabel(severityLabel)
	if err != nil {
		return err
	}
	severityEmoji := severity.SlackEmoji()

	introBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, "An *incident* has been reported.", false, false), nil, nil)
	titleBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: %s %s :loading:", severityEmoji, title), false, false), nil, nil)
	channelBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, ":slack: :loading:", false, false), nil, nil)
	callBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":zoom: <%s>", i.IncidentCallURL), false, false), nil, nil)

	var slackDisplayName, slackProfileImage, gitlabUsername string

	profile, err := i.SlackClient.GetUserProfileContext(ctx, incident.UserID, false)
	if err != nil {
		event.With("get_profile_err", err)
		// Empty string will cause Woodhouse's profile picture not to be overriden.
		// We don't return an error here to allow graceful degradation if the
		// profile can't be fetched.
		// Since we can't fetch the Slack profile, we can't make any assumptions
		// about the GitLab username, so just return the Slack username without any
		// `@` to avoid random GitLab users being notified.
		slackDisplayName = incident.Username
		slackProfileImage = ""
		gitlabUsername = incident.Username
	} else {
		slackDisplayName, slackProfileImage, gitlabUsername = slackutil.GetProfileData(
			ctx, event, profile, i.GitLabProfileSlackFieldID)
	}
	_, msgTimestamp, _, err := i.SlackClient.SendMessageContext(
		ctx, incident.AnnounceChannelID,
		slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock, channelBlock, callBlock),
		slack.MsgOptionUser(incident.UserID),
		slack.MsgOptionUsername(slackDisplayName),
		slack.MsgOptionIconURL(slackProfileImage),

		// This text is used in notifications. If we didn't include it, the
		// notification would say "content cannot be displayed".
		slack.MsgOptionText(fmt.Sprintf("An incident has been reported: %s %s", severityEmoji, title), false),
	)
	if err != nil {
		return err
	}

	// Don't use SendMessageContext to update Slack users. Calls to external
	// systems could themselves time out of their retry loops, and we want to try
	// to update the Slack users anyway.
	updateMsg := func() error {
		_, _, _, err := i.SlackClient.SendMessage(
			incident.AnnounceChannelID,
			slack.MsgOptionUpdate(msgTimestamp),
			slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock, channelBlock, callBlock),
		)
		return err
	}

	sendMsg := func(msg string) error {
		_, _, _, err := i.SlackClient.SendMessage(
			incident.AnnounceChannelID,
			slack.MsgOptionText(msg, false),
		)
		return err
	}

	// Gather all errors and return them at the end, to do as much useful work as
	// possible even if gitlab.com is totally down, preventing us from creating
	// the incident issue.
	var errs []error

	deploymentRelated := false
	for _, labelObject := range payload.View.State.Values["extralabels"]["extralabels"].SelectedOptions {
		if labelObject.Value == "incident-type::deployment related" {
			deploymentRelated = true
			break
		}
	}

	// We only page for severity::1 and severity::2 incidents, and we skip paging
	// if the "deployment related" label is selected, as that label is used for
	// deployment blockers.
	// Those deployment blocker incidents are severity::2 for technical reasons,
	// but do generally not require EOC attention.
	if (severity == 1 || severity == 2) && !deploymentRelated {
		// Tasks correspond 1:1 with a checkbox in the modal. At the moment, all tasks
		// are pagerduty pages.
		for i, err := range i.performOptionalTasks(ctx, payload, incident, title, sendMsg) {
			errs = append(errs, err)
			event.With(fmt.Sprintf("optional_task_err_%d", i), err)
			_ = sendMsg(":x: " + err.Error())
		}
	}

	incidentChannel, err := i.createChannel(ctx, title)
	if err != nil {
		channelBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":slack: :x: %s", err), false, false), nil, nil)
		errs = append(errs, err)
	} else {
		channelBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":slack: <#%s>", incidentChannel.ID), false, false), nil, nil)
	}
	_ = updateMsg()

	// Even if we get typed nil back, we can pass this into createIssue() to
	// create an issue without assignees.
	onCallGitLabUserIDs, err := i.getGitLabUserIDsForOnCalls(ctx)
	if err != nil {
		errs = append(errs, err)
	}

	issue, err := i.createIssue(ctx, event, payload, gitlabUsername, onCallGitLabUserIDs)
	if err != nil {
		_ = sendMsg(":gitlab: :x: Error creating incident issue: " + err.Error())
		_ = sendMsg("Incident issue creation has failed. You might want to follow the workflow for when gitlab.com is totally down: <https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/incidents/README.md#slack-incident-declare-failed-to-create-an-incident-issue>.")
		titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: %s %s", severityEmoji, title), false, false), nil, nil)
		_ = updateMsg()
		return woodhouse.MultiError(append(errs, err))
	}

	if err := i.GitlabGraphQLClient.SetIssueSeverity(
		issue.IID,
		i.GitlabProductionProjectPath,
		severity.IncidentSeverity(),
	); err != nil {
		errs = append(errs, err)
	}

	if err := gitlabutil.TriggerProductionCheckPipeline(
		i.GitlabOpsClient,
		i.GitLabOpsCheckProject,
		i.GitlabOpsCheckTriggerToken,
		issue.IID,
		i.GitlabProductionProjectPath,
	); err != nil {
		errs = append(errs, err)
	}

	titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: <%s|%s %s>", issue.WebURL, severityEmoji, title), false, false), nil, nil)
	_ = updateMsg()

	if incidentChannel != nil {
		newName := i.incidentChannelName(int64(issue.IID))
		if incidentChannel, err = i.SlackClient.RenameConversationContext(ctx, incidentChannel.ID, newName); err != nil {
			errs = append(errs, err)
		}
		if err := i.linkChannelToIssue(ctx, incidentChannel, issue); err != nil {
			errs = append(errs, err)
		}
		if err := i.noteIncidentChannelOnIssue(ctx, issue, incidentChannel, incident); err != nil {
			errs = append(errs, err)
		}
	}

	return woodhouse.MultiError(errs)
}

func (i *IncidentModalHandler) createIssue(
	ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback,
	username string, assigneeIDs []int,
) (*gitlab.Issue, error) {
	title := payload.View.State.Values["title"]["title"].Value
	severity := payload.View.State.Values["severity"]["severity"].SelectedOption.Value
	serviceLabel := payload.View.State.Values["service"]["service"].SelectedOption.Value

	confidential := false
	for _, field := range payload.View.State.Values["confidential"]["confidential"].SelectedOptions {
		switch field.Value {
		case "confidential":
			confidential = true
		}
	}

	var extraLabels []string
	for _, labelObject := range payload.View.State.Values["extralabels"]["extralabels"].SelectedOptions {
		extraLabels = append(extraLabels, labelObject.Value)
	}

	var description bytes.Buffer
	incidentDescriptionTemplate, err := i.getIncidentTemplate(ctx)
	if err != nil {
		return nil, err
	}
	descTmpl := template.Must(template.New("description").Parse(incidentDescriptionTemplate))
	err = descTmpl.Execute(&description, struct {
		Date, Time, Username string
	}{
		Date:     time.Now().UTC().Format("2006-01-02"),
		Time:     time.Now().UTC().Format("15:04"),
		Username: username,
	})
	if err != nil {
		return nil, err
	}
	descStr := description.String()

	var issue *gitlab.Issue
	var issueCreateErrs []string
	err = retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		var err error

		labels := gitlab.Labels{"Source::IMA::IncidentDeclare", severity}
		if serviceLabel != "" {
			labels = append(labels, serviceLabel)
		}
		if extraLabels != nil {
			labels = append(labels, extraLabels...)
		}

		issue, _, err = i.GitlabClient.Issues.CreateIssue(i.GitlabProductionProjectPath, &gitlab.CreateIssueOptions{
			Title:        &title,
			Description:  &descStr,
			Labels:       labels,
			AssigneeIDs:  assigneeIDs,
			IssueType:    &i.GitLabIssueType,
			Confidential: &confidential,
		}, gitlab.WithContext(ctx))
		if err != nil {
			issueCreateErrs = append(issueCreateErrs, err.Error())
		}
		return retry.RetryableError(err)
	})
	if err != nil {
		event.With("issue_create_errs", strings.Join(issueCreateErrs, "\n"))
	}
	return issue, err
}

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func (i *IncidentModalHandler) createChannel(ctx context.Context, issueTitle string) (*slack.Channel, error) {
	// We don't have an issue yet, so use the current time as a placeholder. If we
	// get an issue later, we can update the channel's name.
	issueSlug := time.Now().UTC().Unix()
	chName := i.incidentChannelName(issueSlug)
	incidentChannel, err := i.SlackClient.CreateConversationContext(ctx, chName, false)
	if err != nil {
		return nil, fmt.Errorf("error creating slack channel: %s", err)
	}
	channelPurpose := i.appendIncidentCall(
		fmt.Sprintf(
			`Incident "%s"`,
			woodhouse.TruncateString(issueTitle, issueTitleMaxLengthForSlack),
		),
	)
	_, err = i.SlackClient.SetTopicOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return incidentChannel, fmt.Errorf("error setting channel topic: %s", err)
	}

	return incidentChannel, nil
}

func (i *IncidentModalHandler) incidentChannelName(issueSlug int64) string {
	baseName := i.IncidentChannelNamePrefix
	if baseName == "" {
		projectPathParts := strings.Split(i.GitlabProductionProjectPath, "/")
		projectName := projectPathParts[len(projectPathParts)-1]
		baseName = projectName
	}
	return fmt.Sprintf("%s-%d", baseName, issueSlug)
}

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func (i *IncidentModalHandler) linkChannelToIssue(ctx context.Context, incidentChannel *slack.Channel, issue *gitlab.Issue) error {
	channelPurpose := i.appendIncidentCall(
		fmt.Sprintf(
			`Incident "<%s|%s>"`,
			issue.WebURL,
			woodhouse.TruncateString(issue.Title, issueTitleMaxLengthForSlack)),
	)
	_, err := i.SlackClient.SetPurposeOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return fmt.Errorf("error setting channel purpose: %s", err)
	}
	_, err = i.SlackClient.SetTopicOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return fmt.Errorf("error setting channel topic: %s", err)
	}
	return nil
}

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func (i *IncidentModalHandler) noteIncidentChannelOnIssue(
	ctx context.Context, issue *gitlab.Issue, incidentChannel *slack.Channel, incident IncidentContext,
) error {
	slackChannelIssueComment := fmt.Sprintf("[Slack channel here.](%s)", slackapps.ChannelLink(incidentChannel.ID, incident.TeamID))
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		_, _, err := i.GitlabClient.Notes.CreateIssueNote(
			i.GitlabProductionProjectPath, issue.IID,
			&gitlab.CreateIssueNoteOptions{Body: &slackChannelIssueComment},
			gitlab.WithContext(ctx),
		)
		return retry.RetryableError(err)
	})
	if err != nil {
		return fmt.Errorf("error linking channel in issue comment: %s", err)
	}
	return nil
}

func (i *IncidentModalHandler) performOptionalTasks(
	ctx context.Context, payload *slack.InteractionCallback, incident IncidentContext, title string,
	appendToThread func(string) error,
) []error {
	pagerMsg := fmt.Sprintf("Incident declared: %s", title)
	slackURL := slackapps.ChannelLink(incident.AnnounceChannelID, incident.TeamID)
	var taskErrs []error
	for _, selectedTask := range payload.View.State.Values["tasks"]["tasks"].SelectedOptions {
		switch selectedTask.Value {
		case "pageEOC":
			_ = appendToThread("Paging on-call engineer...")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyEOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging EOC: %s", err))
			}
		case "pageIM":
			_ = appendToThread("Paging IM...")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyIMOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging IM: %s", err))
			}
		case "pageCMOC":
			_ = appendToThread("Paging CMOC...")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyCMOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging CMOC: %s", err))
			}
		default:
			err := fmt.Errorf("unknown task: %s", selectedTask.Value)
			taskErrs = append(taskErrs, err)
		}
	}
	return taskErrs
}

func (i *IncidentModalHandler) appendIncidentCall(msg string) string {
	return fmt.Sprintf("%s\n\n%s", msg, strings.Replace(i.IncidentCallURL, " ", "", -1))
}

func (i *IncidentModalHandler) getGitLabUserIDsForOnCalls(ctx context.Context) ([]int, error) {
	onCallEmails, err := i.Pager.GetOncallEmails(
		ctx,
		[]string{i.PagerdutyEscalationPolicyEOC, i.PagerdutyEscalationPolicyIMOC},
		nil,
		nil,
	)
	if err != nil {
		return nil, err
	}
	return i.getGitLabUserIDs(ctx, onCallEmails)
}

func (i *IncidentModalHandler) getGitLabUserIDs(ctx context.Context, emails []string) ([]int, error) {
	var userIDs []int
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		for _, email := range emails {
			users, _, err := i.GitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{Search: &email}, gitlab.WithContext(ctx))
			if err != nil {
				return retry.RetryableError(err)
			}
			if len(users) != 1 {
				return fmt.Errorf("expected exactly 1 user, got %d for email '%s'", len(users), email)
			}
			userIDs = append(userIDs, users[0].ID)
		}
		return nil
	})
	return userIDs, err
}

func (i *IncidentModalHandler) getIncidentTemplate(ctx context.Context) (string, error) {
	var template string
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		templateBytes, _, err := i.GitlabClient.RepositoryFiles.GetRawFile(
			"gitlab-com/gl-infra/production", ".gitlab/issue_templates/incident.md",
			&gitlab.GetRawFileOptions{Ref: strptr("master")},
			gitlab.WithContext(ctx),
		)
		if err != nil {
			return retry.RetryableError(err)
		}
		template = string(templateBytes)
		return nil
	})
	if err != nil {
		return "", err
	}

	return slackutil.ProcessIssueTemplate(template), nil
}

func strptr(s string) *string {
	return &s
}

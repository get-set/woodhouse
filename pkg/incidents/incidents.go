package incidents

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	ActiveIncidentLabel = "Incident::Active"
)

type Severity int

func ParseSeverityFromIssueLabel(label string) (Severity, error) {
	parts := strings.Split(label, "::")
	if len(parts) != 2 {
		return 0, fmt.Errorf("invalid severity label: %s", label)
	}
	if parts[0] != "severity" {
		return 0, fmt.Errorf("invalid severity label: %s", label)
	}
	sev, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, err
	}
	return Severity(sev), nil
}

func (s Severity) IssueLabel() string {
	return fmt.Sprintf("severity::%d", s)
}

func (s Severity) SlackEmoji() string {
	return fmt.Sprintf(":s%d-incident-severity:", s)
}

func (s Severity) IncidentSeverity() string {
	switch s {
	case 1:
		return "CRITICAL"
	case 2:
		return "HIGH"
	case 3:
		return "MEDIUM"
	case 4:
		return "LOW"
	default:
		return "UNKNOWN"
	}
}

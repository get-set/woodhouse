package changes

import (
	"fmt"
	"strconv"
)

type Criticality int

func ParseCriticalityFromIssueLabel(label string) (Criticality, error) {
	if label[0:1] != "C" {
		return 0, fmt.Errorf("invalid criticality label: %s", label)
	}
	sev, err := strconv.Atoi(label[1:])
	if err != nil {
		return 0, err
	}
	return Criticality(sev), nil
}

func (c Criticality) IssueLabel() string {
	return fmt.Sprintf("C%d", c)
}

func (c Criticality) SlackEmoji() string {
	return fmt.Sprintf(":c%d:", c)
}

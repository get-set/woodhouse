package changes

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"text/template"
	"time"

	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

const (
	ModalCallbackID = "change-declare"
)

var (
	whitespace = regexp.MustCompile(`\s+`)
)

type SlashCommand struct {
	SlackClient                 *slack.Client
	JobRunner                   *woodhouse.AsyncJobRunner
	ChangeChannelID             string
	GitlabProductionProjectPath string
	GitlabGlInfraGroup          string
	GitlabClient                *gitlab.Client
}

type ChangeContext struct {
	UserID            string
	Username          string
	AnnounceChannelID string
	TeamID            string
}

func (s *SlashCommand) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	subcommandArgs := whitespace.Split(command.Text, -1)
	if len(subcommandArgs) != 1 {
		s.help(event, commandName, command, w, req)
		return
	}

	switch subcommandArgs[0] {
	case "declare":
		s.declare(event, command, w, req)
	default:
		s.help(event, commandName, command, w, req)
	}
}

func (s *SlashCommand) declare(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	// Do not alarm the on-call by echoing back the slash command, or posting any
	// messages visible by anyone other than the caller. These would be
	// unactionable until the modal form has been submitted.
	msg := slack.Msg{
		Text:         "I'm on it! Please fill in the form.",
		ResponseType: slack.ResponseTypeEphemeral,
	}
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}

	s.JobRunner.RunAsyncJob("present_change_modal", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return s.presentChangeModal(ctx, event, &command)
	})
}

func (s *SlashCommand) help(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeEphemeral,
		Text:         fmt.Sprintf("Usage: %s declare", commandName),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}

func (s *SlashCommand) presentChangeModal(ctx context.Context, event *instrumentation.Event, command *slack.SlashCommand) error {
	callbackData, err := json.Marshal(ChangeContext{
		UserID:            command.UserID,
		Username:          command.UserName,
		AnnounceChannelID: s.ChangeChannelID,
		TeamID:            command.TeamID,
	})
	if err != nil {
		return err
	}

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slackapps.PlainText("Declare a change"),
		Close:           slackapps.PlainText("Close"),
		Submit:          slackapps.PlainText("Submit"),
		CallbackID:      ModalCallbackID,
		PrivateMetadata: string(callbackData),
	}

	titlePlaceholder := time.Now().UTC().Format("2006-01-02") + ": Repairing widgets"
	titleBlock := slackapps.TextInputBlock("Title", titlePlaceholder)

	criticalityHelpLink := slack.NewTextBlockObject("mrkdwn", "<https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-criticalities|Handbook: Change Criticalities>", false, false)
	criticalityHelpBlock := slack.NewSectionBlock(criticalityHelpLink, nil, nil)
	criticalityBlock := slackapps.SelectInputBlock(
		"Criticality", "Choose a criticality",
		2,
		Criticality(1).IssueLabel(),
		Criticality(2).IssueLabel(),
		Criticality(3).IssueLabel(),
		Criticality(4).IssueLabel(),
	)

	datePickerBlock := slackapps.DatePicker("Due Date")

	serviceLabelNames, err := gitlabutil.GetServiceLabels(s.GitlabClient, s.GitlabProductionProjectPath)
	if err != nil {
		return err
	}

	servicesBlock := slackapps.MultiSelectBlock("Services", "Choose services",
		serviceLabelNames...)

	members, resp, err := s.GitlabClient.Groups.ListGroupMembers(s.GitlabGlInfraGroup, &gitlab.ListGroupMembersOptions{
		ListOptions: gitlab.ListOptions{PerPage: 100},
	})
	if err != nil || resp.StatusCode != 200 {
		return fmt.Errorf("error fetching group members, resp %d: %s", resp.StatusCode, err)
	}

	selectMembers := []string{}
	for _, m := range members {
		if m.Username == "ops-gitlab-net" {
			continue
		}
		selectMembers = append(selectMembers, m.Username)
	}

	reviewBlock := slackapps.SelectInputBlock(
		"Reviewer", "Select a GitLab Username to review the Change",
		-1, // No initial selection
		selectMembers...,
	)

	downtimeBlock := slackapps.SingleCheckboxBlock("Downtime")
	downtimeBlock.Optional = true

	durationBlock := slackapps.TextInputBlock("Duration", "Number of minutes to complete change")
	durationBlock.Optional = true

	confidentialBlock := slack.NewInputBlock(
		"confidential", slackapps.PlainText("Confidential"),
		slack.NewCheckboxGroupsBlockElement(
			"confidential",
			slack.NewOptionBlockObject("confidential", slackapps.PlainText("Mark issue as confidential"), nil),
		),
	)
	confidentialBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, titleBlock, criticalityHelpBlock, criticalityBlock, datePickerBlock, servicesBlock, reviewBlock, durationBlock, downtimeBlock, confidentialBlock)

	_, err = s.SlackClient.OpenView(command.TriggerID, modal)
	if err != nil {
		return fmt.Errorf("error opening modal: %s", err)
	}
	return nil
}

type ChangeModalHandler struct {
	JobRunner                   *woodhouse.AsyncJobRunner
	SlackClient                 *slack.Client
	GitlabClient                *gitlab.Client
	GitlabProductionProjectPath string
	GitLabProfileSlackFieldID   string

	GitlabOpsClient            *gitlab.Client
	GitLabOpsCheckProject      string
	GitlabOpsCheckTriggerToken string
}

func (i *ChangeModalHandler) Handle(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) {
	i.JobRunner.RunAsyncJob("create_change", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return i.createChange(ctx, event, &payload)
	})
}

func (i *ChangeModalHandler) createChange(ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback) error {
	event.With("job", "create_change")

	var change ChangeContext
	if err := json.Unmarshal([]byte(payload.View.PrivateMetadata), &change); err != nil {
		return err
	}
	title := payload.View.State.Values["title"]["title"].Value
	criticalityLabel := payload.View.State.Values["criticality"]["criticality"].SelectedOption.Value
	criticality, err := ParseCriticalityFromIssueLabel(criticalityLabel)
	if err != nil {
		return err
	}
	criticalityEmoji := criticality.SlackEmoji()

	introBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, "A *change* is being created.", false, false), nil, nil)
	titleBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":traffic_cone: %s %s :loading:", criticalityEmoji, title), false, false), nil, nil)

	var slackDisplayName, slackProfileImage, gitlabUsername string

	profile, err := i.SlackClient.GetUserProfileContext(ctx, change.UserID, false)
	if err != nil {
		event.With("get_profile_err", err)
		// Empty string will cause Woodhouse's profile picture not to be overriden.
		// We don't return an error here to allow graceful degradation if the
		// profile can't be fetched.
		// Since we can't fetch the Slack profile, we can't make any assumptions
		// about the GitLab username, so just return the Slack username without any
		// `@` to avoid random GitLab users being notified.
		slackDisplayName = change.Username
		slackProfileImage = ""
		gitlabUsername = change.Username
	} else {
		slackDisplayName, slackProfileImage, gitlabUsername = slackutil.GetProfileData(ctx, event, profile, i.GitLabProfileSlackFieldID)
	}

	_, msgTimestamp, _, err := i.SlackClient.SendMessageContext(
		ctx, change.AnnounceChannelID,
		slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock),
		slack.MsgOptionUser(change.UserID),
		slack.MsgOptionUsername(slackDisplayName),
		slack.MsgOptionIconURL(slackProfileImage),

		// This text is used in notifications. If we didn't include it, the
		// notification would say "content cannot be displayed".
		slack.MsgOptionText(fmt.Sprintf("A change is being created: %s %s", criticalityEmoji, title), false),
	)
	if err != nil {
		return err
	}

	// Don't use SendMessageContext to update Slack users. Calls to external
	// systems could themselves time out of their retry loops, and we want to try
	// to update the Slack users anyway.
	updateMsg := func() error {
		_, _, _, err := i.SlackClient.SendMessage(
			change.AnnounceChannelID,
			slack.MsgOptionUpdate(msgTimestamp),
			slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock),
		)
		return err
	}

	sendMsg := func(msg string) error {
		_, _, _, err := i.SlackClient.SendMessage(
			change.AnnounceChannelID,
			slack.MsgOptionText(msg, false),
		)
		return err
	}

	// Gather all errors and return them at the end, to do as much useful work as
	// possible
	var errs []error

	_ = updateMsg()

	// gitlabUsername has a leading '@' which is needed for the incident command,
	// here it is removed since we don't want it for issue creation
	issue, err := i.createIssue(ctx, event, payload, strings.TrimLeft(gitlabUsername, "@"))
	if err != nil {
		_ = sendMsg(":traffic_cone: :x: Error creating change issue: " + err.Error())
		_ = sendMsg("Change issue creation has failed.")
		titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":traffic_cone: %s %s", criticalityEmoji, title), false, false), nil, nil)
		_ = updateMsg()
		return woodhouse.MultiError(append(errs, err))
	}

	titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":traffic_cone: <%s|%s %s>", issue.WebURL, criticalityEmoji, title), false, false), nil, nil)
	_ = updateMsg()

	return woodhouse.MultiError(errs)
}

func (i *ChangeModalHandler) createIssue(
	ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback,
	username string,
) (*gitlab.Issue, error) {
	title := payload.View.State.Values["title"]["title"].Value
	criticality := payload.View.State.Values["criticality"]["criticality"].SelectedOption.Value
	selectedDate, _ := time.Parse("2006-01-02", payload.View.State.Values["dueDate"]["date"].SelectedDate)
	dueDate := gitlab.ISOTime(selectedDate)
	reviewer := payload.View.State.Values["reviewer"]["reviewer"].SelectedOption.Value

	services := []string{}
	for _, service := range payload.View.State.Values["services"]["services"].SelectedOptions {
		services = append(services, "~\""+service.Value+"\"")
	}
	serviceLabels := strings.Join(services, " ")

	duration := payload.View.State.Values["duration"]["duration"].Value
	if duration == "" {
		duration = "_unknown_"
	} else {
		duration += " minutes"
	}

	downtime := "none"
	if len(payload.View.State.Values["downtime"]["downtime"].SelectedOptions) > 0 {
		downtime = "**YES**"
	}

	confidential := false
	for _, field := range payload.View.State.Values["confidential"]["confidential"].SelectedOptions {
		switch field.Value {
		case "confidential":
			confidential = true
		}
	}

	var description bytes.Buffer
	changeDescriptionTemplate, err := i.getChangeTemplate(ctx)
	if err != nil {
		return nil, err
	}
	descTmpl := template.Must(template.New("description").Parse(changeDescriptionTemplate))

	err = descTmpl.Execute(&description, struct {
		Date, Time, Username, ServiceLabels, Reviewer, Duration, Downtime string
	}{
		Date:          time.Now().UTC().Format("2006-01-02"),
		Time:          time.Now().UTC().Format("15:04"),
		Username:      username,
		ServiceLabels: serviceLabels,
		Reviewer:      reviewer,
		Duration:      duration,
		Downtime:      downtime,
	})
	if err != nil {
		return nil, err
	}
	descStr := description.String()

	var issue *gitlab.Issue
	var issueCreateErrs []string

	err = retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		var err error

		assigneeIDs := []int{}
		users, _, err := i.GitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{
			Username: &username,
		})
		if err != nil {
			issueCreateErrs = append(issueCreateErrs, err.Error())
		}
		if len(users) != 1 {
			issueCreateErrs = append(issueCreateErrs, fmt.Sprintf("expected exactly 1 user, got %d for username'%s'", len(users), username))
		} else {
			assigneeIDs = append(assigneeIDs, users[0].ID)
		}

		issue, _, err = i.GitlabClient.Issues.CreateIssue(i.GitlabProductionProjectPath, &gitlab.CreateIssueOptions{
			Title:        &title,
			Description:  &descStr,
			Labels:       gitlab.Labels{"change", criticality},
			AssigneeIDs:  assigneeIDs,
			DueDate:      &dueDate,
			Confidential: &confidential,
		}, gitlab.WithContext(ctx))
		if err != nil {
			issueCreateErrs = append(issueCreateErrs, err.Error())
		}
		return retry.RetryableError(err)
	})
	if err != nil {
		event.With("issue_create_errs", strings.Join(issueCreateErrs, "\n"))
	}
	return issue, err
}

func (i *ChangeModalHandler) getChangeTemplate(ctx context.Context) (string, error) {
	var template string
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		templateBytes, _, err := i.GitlabClient.RepositoryFiles.GetRawFile(
			"gitlab-com/gl-infra/production", ".gitlab/issue_templates/change_management.md",
			&gitlab.GetRawFileOptions{Ref: strptr("master")},
			gitlab.WithContext(ctx),
		)
		if err != nil {
			return retry.RetryableError(err)
		}
		template = string(templateBytes)
		return nil
	})
	if err != nil {
		return "", err
	}

	return slackutil.ProcessIssueTemplate(template), nil
}

func strptr(s string) *string {
	return &s
}

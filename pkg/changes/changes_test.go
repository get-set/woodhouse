package changes_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/changes"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
)

func TestCriticality(t *testing.T) {
	crit := changes.Criticality(7)
	assert.Equal(t, ":c7:", crit.SlackEmoji())
	assert.Equal(t, "C7", crit.IssueLabel())
}

func TestParseCriticality(t *testing.T) {
	crit, err := changes.ParseCriticalityFromIssueLabel("C42")
	assert.Nil(t, err)
	assert.Equal(t, ":c42:", crit.SlackEmoji())
}

func TestParseCriticality_returnsErrorWhenWrongLabelPassed(t *testing.T) {
	_, err := changes.ParseCriticalityFromIssueLabel("foo::2")
	assert.NotNil(t, err)
	_, err = changes.ParseCriticalityFromIssueLabel("bar")
	assert.NotNil(t, err)
	_, err = changes.ParseCriticalityFromIssueLabel("foo::1::2")
	assert.NotNil(t, err)
}

func TestParseGitLabHandleFromURL(t *testing.T) {
	for _, tc := range []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "returns username when input is a valid GitLab profile URL",
			input:    "https://gitlab.com/a_profile-with.all_valid_chars123",
			expected: "@a_profile-with.all_valid_chars123",
		},
		{
			name:     "returns empty string when input is not a valid GitLab profile URL",
			input:    "https://github.com/craigfurman",
			expected: "",
		},
		{
			name:     "returns empty string when input is not a valid URL",
			input:    "foo",
			expected: "",
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handle := slackutil.ParseGitLabHandleFromURL(tc.input)
			assert.Equal(t, tc.expected, handle)
		})
	}
}

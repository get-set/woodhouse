package slackutil

import (
	"context"
	"regexp"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

var (
	regexWoodhouseReplace = regexp.MustCompile("(?m)<!-- woodhouse: '(.*)' -->.*$")
	regexGitLabProfile    = regexp.MustCompile(`^https://gitlab\.com/([\w\-_\.]+)$`)
)

func ProcessIssueTemplate(rawTemplate string) string {
	return regexWoodhouseReplace.ReplaceAllString(rawTemplate, "$1")
}

func GetProfileData(
	ctx context.Context,
	event *instrumentation.Event,
	profile *slack.UserProfile,
	gitLabProfileSlackFieldID string,
) (slackDisplayName, slackProfileImage, gitlabUsername string) {
	slackDisplayName = getSlackDisplayName(profile)

	slackProfileImage, size := getSlackProfileImage(profile)
	event.With("image_size", size)

	gitlabUsername = getGitLabHandle(profile, gitLabProfileSlackFieldID)
	if gitlabUsername == "" {
		gitlabUsername = slackDisplayName
	}

	return slackDisplayName, slackProfileImage, gitlabUsername
}

func getGitLabHandle(profile *slack.UserProfile, gitlabProfileSlackFieldID string) string {
	gitlabProfileURL, ok := profile.Fields.ToMap()[gitlabProfileSlackFieldID]
	if !ok {
		return ""
	}
	return ParseGitLabHandleFromURL(gitlabProfileURL.Value)
}

func getSlackDisplayName(profile *slack.UserProfile) string {
	if profile.DisplayName != "" {
		return profile.DisplayName
	}
	return profile.RealName
}

func getSlackProfileImage(profile *slack.UserProfile) (imageURL, size string) {
	if profile.ImageOriginal != "" {
		return profile.ImageOriginal, "original"
	} else if profile.Image192 != "" {
		return profile.Image192, "192"
	} else if profile.Image72 != "" {
		return profile.Image72, "72"
	} else if profile.Image48 != "" {
		return profile.Image48, "48"
	} else if profile.Image32 != "" {
		return profile.Image32, "32"
	} else if profile.Image24 != "" {
		return profile.Image24, "24"
	}

	return "", ""
}

func ParseGitLabHandleFromURL(url string) string {
	matches := regexGitLabProfile.FindStringSubmatch(url)
	if len(matches) < 2 {
		return ""
	}
	return "@" + matches[1]
}

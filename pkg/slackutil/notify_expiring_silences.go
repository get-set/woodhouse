package slackutil

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/alertmanager"
	"strings"
	"time"
)

type NotifyExpiringSilencesOptions struct {
	Silences                    []alertmanager.Silence
	SlackClient                 *slack.Client
	SlackChannelID              string
	AlertmanagerExternalBaseURL string
	Cutoff                      time.Duration
	DryRun                      bool
}

func NotifyExpiringSilences(ctx context.Context, logger log.Logger, options NotifyExpiringSilencesOptions) error {
	if options.DryRun {
		for _, silence := range options.Silences {
			logger.Log(
				"msg", "silence expiring soon", "id", silence.ID, "cutoff", options.Cutoff,
				"comment", silence.Comment, "created_by", silence.CreatedBy,
				"starts_at", silence.StartsAt, "ends_at", silence.EndsAt,
			)
		}
		return nil
	}

	if len(options.Silences) == 0 {
		return nil
	}

	var blocks []slack.Block

	title := fmt.Sprintf(":warning: @sre-oncall The following silences are about to expire over the next %s.", options.Cutoff)
	blocks = append(blocks, slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, title, false, false), nil, nil))
	blocks = append(blocks, slack.NewDividerBlock())

	for _, silence := range options.Silences {
		silenceURL := fmt.Sprintf("%s/#/silences/%s", options.AlertmanagerExternalBaseURL, silence.ID)
		silenceID := silence.ID
		if strings.Contains(silenceID, "-") {
			silenceID = strings.Split(silenceID, "-")[0]
		}
		silenceMsg := fmt.Sprintf(
			"• `%s` - <%s|*`%s`*> created by *%s* with comment %s",
			silence.EndsAt.Format("2006-01-02 15:04 MST"),
			silenceURL, silenceID, silence.CreatedBy, silence.Comment,
		)
		blocks = append(blocks, slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, silenceMsg, false, false), nil, nil))
	}

	_, _, _, err := options.SlackClient.SendMessageContext(
		ctx,
		options.SlackChannelID,
		slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionBlocks(blocks...),

		// This text is used in notifications. If we didn't include it, the
		// notification would say "content cannot be displayed".
		slack.MsgOptionText(title, false),
	)
	if err != nil {
		return err
	}

	return nil
}

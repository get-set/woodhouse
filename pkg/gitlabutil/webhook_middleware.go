package gitlabutil

import (
	"crypto/subtle"
	"net/http"
)

func NewGitLabWebhookMiddleware(secretToken string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if subtle.ConstantTimeCompare([]byte(secretToken), []byte(req.Header.Get("X-Gitlab-Token"))) == 0 {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, req)
	})
}

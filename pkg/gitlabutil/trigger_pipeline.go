package gitlabutil

import (
	"strconv"

	"github.com/xanzy/go-gitlab"
)

func TriggerProductionCheckPipeline(client *gitlab.Client, project string, token string, iid int, productionProjectPath string) error {
	_, _, err := client.PipelineTriggers.RunPipelineTrigger(
		project,
		&gitlab.RunPipelineTriggerOptions{
			Ref:   strptr("master"),
			Token: strptr(token),
			Variables: map[string]string{
				"ref":                            "master",
				"PRODUCTION_ISSUE_IID":           strconv.Itoa(iid),
				"CHECK_PRODUCTION":               "true",
				"GITLAB_PRODUCTION_PROJECT_PATH": productionProjectPath,
			},
		},
	)
	return err
}

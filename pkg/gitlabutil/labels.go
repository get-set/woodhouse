package gitlabutil

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

func GetServiceLabels(client *gitlab.Client, projectPath string) ([]string, error) {
	serviceLabelNames := []string{}

	labelOpt := &gitlab.ListLabelsOptions{
		Search: strptr("Service::"),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		serviceLabels, resp, err := client.Labels.ListLabels(projectPath, labelOpt)
		if err != nil || resp.StatusCode != 200 {
			return serviceLabelNames, fmt.Errorf("error fetching service labels: %s", err)
		}

		for _, l := range serviceLabels {
			serviceLabelNames = append(serviceLabelNames, l.Name)
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		labelOpt.Page = resp.NextPage
	}

	return serviceLabelNames, nil
}

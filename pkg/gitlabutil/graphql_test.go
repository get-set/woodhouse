package gitlabutil

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSetIssueSeverity(t *testing.T) {
	expected := `{"data":{"issueSetSeverity":{"errors":[]}}}`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		if r.Method != "POST" {
			t.Errorf("Expected 'POST' request, got '%s'", r.Method)
		}

		fmt.Fprint(w, expected)
	}))

	defer ts.Close()

	graphQLClient := NewGraphQLClient(
		ts.Client(),
		"some-token",
		ts.URL,
	)

	err := graphQLClient.SetIssueSeverity(100, "some/project/path", "SOME_SEVERITY")
	require.Nil(t, err)
}

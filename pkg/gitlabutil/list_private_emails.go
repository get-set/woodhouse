package gitlabutil

import (
	"context"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"

	"github.com/xanzy/go-gitlab"
)

func dedup(strs []string) []string {
	seen := make(map[string]bool)
	out := []string{}
	for _, str := range strs {
		if _, v := seen[str]; !v {
			seen[str] = true
			out = append(out, str)
		}
	}
	return out
}

func ListPrivateEmails(ctx context.Context, pdClient *gopagerduty.Client, gitlabClient *gitlab.Client, escalationPolicies []string) ([]string, error) {
	since := time.Now().Add(-30 * 24 * time.Hour)
	until := time.Now().Add(24 * time.Hour)

	var onCallEmails []string
	for offset := uint(0); ; offset += 100 {
		onCalls, err := pdClient.ListOnCalls(gopagerduty.ListOnCallOptions{
			EscalationPolicyIDs: escalationPolicies,
			Since:               since.Format(time.RFC3339),
			Until:               until.Format(time.RFC3339),
			Includes:            []string{"users"},
			APIListObject: gopagerduty.APIListObject{
				Offset: offset,
				Limit:  100,
			},
		})
		if err != nil {
			return nil, err
		}
		if !onCalls.More {
			break
		}
		for _, onCall := range onCalls.OnCalls {
			// Only take the first on-call
			if onCall.EscalationLevel != 1 {
				continue
			}
			if onCall.User.Email == "" {
				continue
			}
			onCallEmails = append(onCallEmails, onCall.User.Email)
		}
	}

	onCallEmails = dedup(onCallEmails)

	var privateEmails []string
	for _, email := range onCallEmails {
		users, _, err := gitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{Search: &email}, gitlab.WithContext(ctx))
		if err != nil {
			return nil, err
		}
		if len(users) == 0 {
			privateEmails = append(privateEmails, email)
		}
	}
	return privateEmails, nil
}

// Based on https://gitlab.com/cross-project-pipelines/cross-project-pipelines

package gitlabutil

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/sethvargo/go-retry"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

func FollowRemotePipeline(ctx context.Context, logger log.Logger, client *gitlab.Client, projectPath, commitSHA string) error {
	logger = log.With(logger, "project_path", projectPath, "commit_sha", commitSHA)

	pipelines, err := awaitNonzeroPipelineCount(ctx, client, projectPath, commitSHA)
	if err != nil {
		return err
	}
	pipelineStatuses := make(chan *gitlab.Pipeline)
	for _, pipeline := range pipelines {
		logger.Log("msg", "monitoring pipeline", "pipeline", pipeline.WebURL)
		go monitorPipeline(ctx, client, projectPath, pipeline, pipelineStatuses)
	}

	pipelinesRemaining := len(pipelines)
	var errs []error

	for {
		select {
		case pipelineComplete := <-pipelineStatuses:
			logger.Log("msg", "pipeline completed", "pipeline", pipelineComplete.WebURL, "status", pipelineComplete.Status)
			if !isSuccessStatus(pipelineComplete.Status) {
				errs = append(errs, fmt.Errorf("pipeline %s finished with status %s", pipelineComplete.WebURL, pipelineComplete.Status))
			}
			pipelinesRemaining--
			if pipelinesRemaining == 0 {
				return woodhouse.MultiError(errs)
			}
		case <-ctx.Done():
			errs = append(errs, ctx.Err())
			return woodhouse.MultiError(errs)
		}
	}
}

func monitorPipeline(
	ctx context.Context, client *gitlab.Client, projectPath string,
	pipeline *gitlab.PipelineInfo, pipelineStatuses chan<- *gitlab.Pipeline,
) {
	var latestPipeline *gitlab.Pipeline
	_ = retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		latestPipeline, _, err = client.Pipelines.GetPipeline(projectPath, pipeline.ID)
		if err != nil {
			return retry.RetryableError(err)
		}
		if !isTerminalStatus(latestPipeline.Status) {
			return retry.RetryableError(fmt.Errorf("pipeline %s status: %s", latestPipeline.WebURL, latestPipeline.Status))
		}
		return nil
	})
	pipelineStatuses <- latestPipeline
}

func awaitNonzeroPipelineCount(ctx context.Context, client *gitlab.Client, projectPath, commitSHA string) ([]*gitlab.PipelineInfo, error) {
	var pipelines []*gitlab.PipelineInfo
	err := retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		pipelines, _, err = client.Pipelines.ListProjectPipelines(projectPath, &gitlab.ListProjectPipelinesOptions{SHA: &commitSHA})
		if err != nil {
			return retry.RetryableError(err)
		}
		if len(pipelines) == 0 {
			return retry.RetryableError(errors.New("zero pipelines found"))
		}
		return nil
	})
	return pipelines, err
}

// https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
func isTerminalStatus(pipelineStatus string) bool {
	for _, terminalStatus := range []string{"success", "failed", "canceled", "skipped", "manual"} {
		if pipelineStatus == terminalStatus {
			return true
		}
	}
	return false
}

func isSuccessStatus(pipelineStatus string) bool {
	for _, successStatus := range []string{"success", "manual"} {
		if pipelineStatus == successStatus {
			return true
		}
	}
	return false
}

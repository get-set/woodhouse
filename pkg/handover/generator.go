package handover

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"text/template"
	"time"

	"github.com/go-kit/kit/log/level"

	pagerduty2 "github.com/PagerDuty/go-pagerduty"

	"github.com/go-kit/kit/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
)

// port of https://ops.gitlab.net/gitlab-com/gl-infra/sre-oncall-handover-automation

var issueDescription = `# On-Call Handover

*Brought to you by woodhouse*

<!--
https://gitlab.com/gitlab-com/gl-infra/woodhouse/-/blob/master/pkg/handover/generator.go
-->

{{ define "GitLabUsername" }}
{{- if not .UsernameLookupFailed -}}@{{ end }}{{ .Username -}}
{{ end }}

- **EOC egress:** {{ range .EgressOnCalls }}{{ template "GitLabUsername" . }} {{ end }}
- **EOC ingress:** {{ range .IngressOnCalls }}{{ template "GitLabUsername" . }} {{ end }}
- **IM egress:** {{ range .EgressIncidentManagers }}{{ template "GitLabUsername" . }} {{ end }}
- **IM ingress:** {{ range .IngressIncidentManagers }}{{ template "GitLabUsername" . }} {{ end }}

{{ if .AssignUsernames }}
/assign {{ range .AssignUsernames }}@{{ . }} {{ end }}
{{ end }}

/label ~"SRE:On-Call"

## :book: Summary:

#### What (if any) time-critical work is being handed over?

<!--
Please list any urgent tasks that cannot wait, such as mitigating an active incident
or preventing imminent breakage.
-->

#### What contextual info may be useful for the next few on-call shifts?

<!--
Please help the incoming on-callers to efficiently interpret symptoms or alerts.  For example:

* Are any system components known to be in an abnormal, degraded, or risky state?
* Do you think any recent problems may recur or have delayed or lingering side effects?
* If a significant incident occurred, can you briefly summarize its current status?
-->

{{ define "GitLabIssueSeverity" -}}
{{- range .Labels -}}
	{{- if hasPrefix . "severity::" -}}
		~"{{ . }}"
	{{- end -}}
{{- end -}}
{{- end }}

{{ define "GitLabIssueListItem" }}
{{- if .Confidential -}}
- {{ .WebURL }}
{{- else -}}
- {{ .WebURL }} - {{ template "GitLabIssueSeverity" . }} {{ .Title }}
{{- end }}
{{ end }}

## :red_circle: Ongoing alerts/incidents:
{{ if .OngoingGitLabIncidents }}
### GitLab
{{ range .OngoingGitLabIncidents }}
	{{- template "GitLabIssueListItem" . -}}
{{ end }}
{{ end }}

{{ if .OngoingPagerDutyIncidents }}
### PagerDuty
{{ range .OngoingPagerDutyIncidents }}
- {{ .HTMLURL }} - {{ .Summary }}
{{ end }}
{{ end }}

## :white_check_mark: Resolved alerts/incidents:
{{ if .OngoingGitLabIncidents }}
### GitLab
{{ range .ResolvedGitLabIncidents }}
	{{- template "GitLabIssueListItem" . -}}
{{ end }}
{{ end }}

{{ if .OngoingPagerDutyIncidents }}
### PagerDuty
{{ range .ResolvedPagerDutyIncidents }}
- {{ .HTMLURL }} - {{ .Summary }}
{{ end }}
{{ end }}

{{ if .MitigatedGitLabIncidents }}
## :large_blue_circle: Mitigated incidents:
{{ range .MitigatedGitLabIncidents }}
	{{- template "GitLabIssueListItem" . -}}
{{ end }}
{{ end }}

## :unlock: Change issues:

{{ if .InProgressChangeRequests }}
#### In Progress
{{ range .InProgressChangeRequests }}
	{{- template "GitLabIssueListItem" . -}}
{{ end }}
{{ end }}

{{ if .ClosedChangeRequests }}
#### Closed
{{ range .ClosedChangeRequests }}
	{{- template "GitLabIssueListItem" . -}}
{{ end }}
{{ end }}
`

type HandoverIssueData struct {
	ShiftStart string
	ShiftEnd   string

	EgressOnCalls           []GitLabUsername
	IngressOnCalls          []GitLabUsername
	EgressIncidentManagers  []GitLabUsername
	IngressIncidentManagers []GitLabUsername

	AssignUsernames []string

	OngoingGitLabIncidents   []*gitlab.Issue
	ResolvedGitLabIncidents  []*gitlab.Issue
	MitigatedGitLabIncidents []*gitlab.Issue

	InProgressChangeRequests []*gitlab.Issue
	ClosedChangeRequests     []*gitlab.Issue

	OngoingPagerDutyIncidents  []pagerduty2.Incident
	ResolvedPagerDutyIncidents []pagerduty2.Incident
}

type GitLabUsername struct {
	Username             string
	Err                  error
	UsernameLookupFailed bool
}

type Generator struct {
	Pager                         *pagerduty.Pager
	GitlabClient                  *gitlab.Client
	Logger                        log.Logger
	PagerdutyEscalationPolicyEOC  string
	PagerdutyEscalationPolicyIMOC string
	PagerdutyServiceIDs           []string
	ProductionProjectPath         string
}

func (g *Generator) getGitLabUsername(ctx context.Context, email string) (string, error) {
	users, _, err := g.GitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{Search: &email}, gitlab.WithContext(ctx))
	if err != nil {
		return "", err
	}
	if len(users) != 1 {
		return "", fmt.Errorf("expected exactly 1 user, got %d for email '%s'", len(users), email)
	}
	return users[0].Username, nil
}

func (g *Generator) getGitLabUsernames(ctx context.Context, emails []string) []GitLabUsername {
	var usernames []GitLabUsername
	for _, email := range emails {
		username, err := g.getGitLabUsername(ctx, email)
		if err != nil {
			level.Warn(g.Logger).Log("action", "handover", "msg", "failed to resolve email to gitlab username", "email", email, "err", err)
			username = strings.Split(email, "@")[0]
			usernames = append(usernames, GitLabUsername{
				Username:             username,
				Err:                  err,
				UsernameLookupFailed: true,
			})
		} else {
			usernames = append(usernames, GitLabUsername{
				Username: username,
			})
		}
	}
	return usernames
}

func (g *Generator) getGitLabUsernamesFromEscalationPolicies(ctx context.Context, escalationPolicies []string, since, until *time.Time) ([]GitLabUsername, error) {
	onCallEmails, err := g.Pager.GetOncallEmails(
		ctx,
		escalationPolicies,
		since,
		until,
	)
	if err != nil {
		return nil, err
	}

	// getGitLabUsernames handles and logs errors internally
	usernames := g.getGitLabUsernames(ctx, onCallEmails)

	return usernames, nil
}

// Generate generates title and body for the handover
// We expect the handover to be generated _just under_ 1 hour before end of shift via CronJob.
// To determine the egress on-calls, we look back 1 hour to see who is currently on-call.
// To determine the ingress on-calls, we look forward 1 hour into the future to see who
// will be on-call.
func (g *Generator) Generate(ctx context.Context, now time.Time) (string, string, error) {
	egressSince := now.Add(-1 * time.Hour)
	egressUntil := now

	shiftStart, shiftEnd, err := g.Pager.GetOncallShiftStartAndEndTime(
		ctx,
		[]string{g.PagerdutyEscalationPolicyEOC},
		&egressSince,
		&egressUntil,
	)
	if err != nil {
		return "", "", err
	}

	shiftStartTime, err := time.Parse(time.RFC3339, shiftStart)
	if err != nil {
		return "", "", err
	}

	egressOnCalls, err := g.getGitLabUsernamesFromEscalationPolicies(
		ctx,
		[]string{g.PagerdutyEscalationPolicyEOC},
		&egressSince,
		&egressUntil,
	)
	if err != nil {
		return "", "", err
	}

	egressIncidentManagers, err := g.getGitLabUsernamesFromEscalationPolicies(
		ctx,
		[]string{g.PagerdutyEscalationPolicyIMOC},
		&egressSince,
		&egressUntil,
	)
	if err != nil {
		return "", "", err
	}

	ingressSince := now.Add(1 * time.Hour)
	ingressUntil := now.Add(2 * time.Hour)
	ingressOnCalls, err := g.getGitLabUsernamesFromEscalationPolicies(
		ctx,
		[]string{g.PagerdutyEscalationPolicyEOC},
		&ingressSince,
		&ingressUntil,
	)
	if err != nil {
		return "", "", err
	}

	ingressIncidentManagers, err := g.getGitLabUsernamesFromEscalationPolicies(
		ctx,
		[]string{g.PagerdutyEscalationPolicyIMOC},
		&ingressSince,
		&ingressUntil,
	)
	if err != nil {
		return "", "", err
	}

	var assignUsernames []string
	for _, u := range ingressOnCalls {
		if !u.UsernameLookupFailed {
			assignUsernames = append(assignUsernames, u.Username)
		}
	}
	for _, u := range ingressIncidentManagers {
		if !u.UsernameLookupFailed {
			assignUsernames = append(assignUsernames, u.Username)
		}
	}

	ongoingIssues, _, err := g.GitlabClient.Issues.ListProjectIssues(
		g.ProductionProjectPath,
		&gitlab.ListProjectIssuesOptions{
			Labels: []string{"incident", "Incident::Active"},
			State:  gitlab.String("opened"),
		},
	)
	if err != nil {
		return "", "", err
	}

	resolvedIssues, _, err := g.GitlabClient.Issues.ListProjectIssues(
		g.ProductionProjectPath,
		&gitlab.ListProjectIssuesOptions{
			Labels:       []string{"incident"},
			NotLabels:    []string{"Incident::Active"},
			CreatedAfter: &shiftStartTime,
		},
	)
	if err != nil {
		return "", "", err
	}

	mitigatedIssues, _, err := g.GitlabClient.Issues.ListProjectIssues(
		g.ProductionProjectPath,
		&gitlab.ListProjectIssuesOptions{
			Labels:       []string{"incident", "Incident::Mitigated"},
			State:        gitlab.String("opened"),
			CreatedAfter: &shiftStartTime,
		},
	)
	if err != nil {
		return "", "", err
	}

	inProgressChangeRequests, _, err := g.GitlabClient.Issues.ListProjectIssues(
		g.ProductionProjectPath,
		&gitlab.ListProjectIssuesOptions{
			Labels:       []string{"change", "change::in-progress"},
			State:        gitlab.String("opened"),
			UpdatedAfter: &shiftStartTime,
		},
	)
	if err != nil {
		return "", "", err
	}

	closedChangeRequests, _, err := g.GitlabClient.Issues.ListProjectIssues(
		g.ProductionProjectPath,
		&gitlab.ListProjectIssuesOptions{
			Labels:       []string{"change"},
			State:        gitlab.String("closed"),
			UpdatedAfter: &shiftStartTime,
		},
	)
	if err != nil {
		return "", "", err
	}

	pdResp, err := g.Pager.PagerdutyClient.ListIncidents(pagerduty2.ListIncidentsOptions{
		Statuses:   []string{"triggered", "acknowledged"},
		ServiceIDs: g.PagerdutyServiceIDs,
		Urgencies:  []string{"high"},
		SortBy:     "created_at",
	})
	if err != nil {
		return "", "", err
	}
	ongoingPagerdutyIncidents := pdResp.Incidents

	pdResp, err = g.Pager.PagerdutyClient.ListIncidents(pagerduty2.ListIncidentsOptions{
		Since:      shiftStart,
		Until:      shiftEnd,
		Statuses:   []string{"resolved"},
		ServiceIDs: g.PagerdutyServiceIDs,
		Urgencies:  []string{"high"},
		SortBy:     "created_at",
	})
	if err != nil {
		return "", "", err
	}
	resolvedPagerdutyIncidents := pdResp.Incidents

	funcMap := template.FuncMap{
		"hasPrefix": strings.HasPrefix,
	}

	tmpl, err := template.New("handover_issue").Funcs(funcMap).Parse(issueDescription)
	if err != nil {
		return "", "", err
	}

	data := &HandoverIssueData{
		ShiftStart: shiftStart,
		ShiftEnd:   shiftEnd,

		EgressOnCalls:           egressOnCalls,
		IngressOnCalls:          ingressOnCalls,
		EgressIncidentManagers:  egressIncidentManagers,
		IngressIncidentManagers: ingressIncidentManagers,

		AssignUsernames: assignUsernames,

		OngoingGitLabIncidents:   ongoingIssues,
		ResolvedGitLabIncidents:  resolvedIssues,
		MitigatedGitLabIncidents: mitigatedIssues,

		InProgressChangeRequests: inProgressChangeRequests,
		ClosedChangeRequests:     closedChangeRequests,

		OngoingPagerDutyIncidents:  ongoingPagerdutyIncidents,
		ResolvedPagerDutyIncidents: resolvedPagerdutyIncidents,
	}

	buf := bytes.NewBuffer([]byte{})
	err = tmpl.Execute(buf, data)
	if err != nil {
		return "", "", err
	}

	titleTime := shiftEnd
	titleTime = strings.ReplaceAll(titleTime, "T", " ")
	titleTime = strings.ReplaceAll(titleTime, ":00Z", "")

	title := fmt.Sprintf("On-Call Handover %s UTC", titleTime)

	return buf.String(), title, nil
}

package woodhouse

import (
	"context"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

var (
	asyncJobsTotal          *prometheus.CounterVec
	asyncJobDurationSeconds *prometheus.HistogramVec
)

type Job func(ctx context.Context) error

type AsyncJobRunner struct {
	jobTimeout    time.Duration
	logger        log.Logger
	threadCounter *sync.WaitGroup
}

func NewAsyncJobRunner(logger log.Logger, metricsRegistry prometheus.Registerer, jobTimeout time.Duration, threadCounter *sync.WaitGroup) *AsyncJobRunner {
	factory := promauto.With(metricsRegistry)

	asyncJobsTotal = factory.NewCounterVec(prometheus.CounterOpts{
		Namespace: "woodhouse",
		Name:      "async_jobs_total",
		Help:      "Number of async jobs completed.",
	}, []string{"async_job_name", "status"})

	asyncJobDurationSeconds = factory.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "woodhouse",
		Name:      "async_job_duration_seconds",
		Help:      "Durations of async jobs.",
		Buckets:   []float64{.1, .25, .5, 1, 5, 10, 30, 60, 120, 300},
	}, []string{"async_job_name", "status"})

	return &AsyncJobRunner{logger: logger, jobTimeout: jobTimeout, threadCounter: threadCounter}
}

func (r *AsyncJobRunner) RunAsyncJob(name string, job Job) {
	ctx, cancel := context.WithTimeout(context.Background(), r.jobTimeout)
	event := instrumentation.NewEvent()
	event.With("async_job_name", name)
	ctx = context.WithValue(ctx, instrumentation.EventContextKey{}, event)
	r.threadCounter.Add(1)

	go func() {
		defer r.threadCounter.Done()
		defer cancel()

		var err error
		duration := instrumentation.MeasureTime(func() {
			err = job(ctx)
		})
		if err == nil {
			event.With("status", "success")
		} else {
			event.With("status", "error", "error", err)
		}

		asyncJobsTotal.WithLabelValues(name, event.Get("status").(string)).Inc()
		asyncJobDurationSeconds.WithLabelValues(name, event.Get("status").(string)).Observe(duration)

		event.With("event", "async_job_complete", "duration_s", duration)
		r.logger.Log(event.All()...)
	}()
}

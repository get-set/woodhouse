package woodhouse

func TruncateString(s string, maxLength int) string {
	chars := []rune(s)
	if len(chars) <= maxLength || maxLength <= 3 {
		return s
	}
	return string(chars[:(maxLength-3)]) + "..."
}

package woodhouse_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

func TestTruncateString(t *testing.T) {
	for _, tc := range []struct {
		name            string
		input, expected string
		max             int
	}{
		{
			name:     "returns string if shorter than max length",
			input:    "foo-bar",
			expected: "foo-bar",
			max:      7,
		},
		{
			name:     "returns truncated string with ellipsis if longer than max length",
			input:    "0123456789",
			expected: "012345...",
			max:      9,
		},
		{
			name:     "returns whole string if max<=3",
			input:    "abcd",
			expected: "abcd",
			max:      3,
		},
		{
			name:     "returns truncated string with ellipsis if longer than max length and contains characters longer than 1 byte",
			input:    "世界 世界 世界 世界 世界",
			expected: "世界 世...",
			max:      7,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			result := woodhouse.TruncateString(tc.input, tc.max)
			assert.Equal(t, tc.expected, result)
		})
	}
}

package instrumentation_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

func TestEvent(t *testing.T) {
	for _, tc := range []struct {
		name   string
		input  []interface{}
		output []interface{}
	}{
		{
			name:  "returns a single key-value pair of strings",
			input: []interface{}{"foo", "bar"},
		},
		{
			name:  "returns a single key-value pair with non-string value",
			input: []interface{}{"foo", 10},
		},
		{
			name:  "returns several key-value pairs",
			input: []interface{}{"foo", 10, "bar", "baz"},
		},
		{
			name:   "doesn't panic when given a non-string key",
			input:  []interface{}{"works1", "val1", 10, "foo", "works2", "val2"},
			output: []interface{}{"works1", "val1", "works2", "val2"},
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			event := instrumentation.NewEvent()
			event.With(tc.input...)

			expectedOutput := tc.output
			if expectedOutput == nil {
				expectedOutput = tc.input
			}
			assert.ElementsMatch(t, expectedOutput, event.All())
		})
	}
}

package instrumentation_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

func TestInstrumentationMiddleware_ItCallsTheNextHandler(t *testing.T) {
	server := setupInstrumentationMiddleware(&bytes.Buffer{})
	defer server.Close()

	resp, err := server.Client().Get(server.URL)
	require.Nil(t, err)
	defer resp.Body.Close()

	assert.Equal(t, 202, resp.StatusCode)
	respBytes, err := io.ReadAll(resp.Body)
	require.Nil(t, err)
	assert.Equal(t, "a response", string(respBytes))
}

func TestInstrumentationMiddleware_ItLogsTheEventWithStatusAndDuration(t *testing.T) {
	var logBuf bytes.Buffer
	server := setupInstrumentationMiddleware(&logBuf)
	defer server.Close()

	resp, err := server.Client().Get(server.URL + "/path")
	require.Nil(t, err)
	defer resp.Body.Close()

	var event map[string]interface{}
	require.Nil(t, json.Unmarshal(logBuf.Bytes(), &event))

	assert.Equal(t, "http_request", event["event"])
	assert.Equal(t, "bar", event["foo"])
	assert.Equal(t, float64(202), event["status"])
	assert.Equal(t, "/path", event["path"])
	assert.Equal(t, "GET", event["method"])
	assert.Greater(t, event["duration_s"], float64(0))
}

func TestInstrumentationMiddleware_ItDoesntLogSpecifiedPaths(t *testing.T) {
	var logBuf bytes.Buffer
	server := setupInstrumentationMiddleware(&logBuf)
	defer server.Close()

	resp, err := server.Client().Get(server.URL + "/dontlog")
	require.Nil(t, err)
	defer resp.Body.Close()

	assert.Equal(t, 0, len(logBuf.Bytes()))
}

func setupInstrumentationMiddleware(logBuf io.Writer) *httptest.Server {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		event := r.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		event.With("foo", "bar")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprint(w, "a response")
	})

	logger := log.NewJSONLogger(log.NewSyncWriter(logBuf))
	middleware := instrumentation.NewInstrumentedHTTPMiddleware(logger, prometheus.NewRegistry(), []string{"/dontlog"}, nextHandler)
	return httptest.NewServer(middleware)
}

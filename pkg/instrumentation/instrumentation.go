package instrumentation

import (
	"sync"
	"time"
)

type Event struct {
	event map[string]interface{}
	mutex *sync.RWMutex
}

func NewEvent() *Event {
	return &Event{event: map[string]interface{}{}, mutex: &sync.RWMutex{}}
}

func (e *Event) With(keysAndVals ...interface{}) {
	e.mutex.Lock()
	defer e.mutex.Unlock()

	for i := 0; i < len(keysAndVals)-1; i += 2 {
		strKey, ok := keysAndVals[i].(string)
		if !ok {
			continue
		}
		e.event[strKey] = keysAndVals[i+1]
	}
}

func (e *Event) Get(key string) interface{} {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	return e.event[key]
}

func (e *Event) All() []interface{} {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	keysAndVals := make([]interface{}, len(e.event)*2)
	i := 0
	for key, val := range e.event {
		keysAndVals[i] = key
		keysAndVals[i+1] = val
		i += 2
	}
	return keysAndVals
}

func MeasureTime(op func()) float64 {
	start := time.Now()
	op()
	return float64(time.Since(start)) / float64(time.Second)
}

#!/usr/bin/env bash
set -euo pipefail

# To use, configure the following CI vars:
# GITLAB_PUSH_TOKEN: an API token with api and write_repository scopes
# GITLAB_USERNAME: Username to pass to `git config user.name`
# GITLAB_EMAIL: Username to pass to `git config user.email`
# ASSIGNEES: Space-separated list of GitLab.com usernames to assign the merge request to

# Allow this script to run even when updating dependencies breaks compilation.
go get -u ./... || true

go mod tidy
if git diff --quiet; then
  echo "Dependencies already up to date, exitting."
  exit 0
fi

update_branch="auto-update-$(date +%s)"
git config user.name "${GITLAB_USERNAME}"
git config user.email "${GITLAB_EMAIL}"
git checkout -b "${update_branch}"
git commit -am 'Auto-update dependencies'
git remote set-url --push origin "https://oauth2:${GITLAB_PUSH_TOKEN}@gitlab.com/${CI_PROJECT_PATH}"
git push origin "${update_branch}"

curl_flags=(
  -X POST -H "Authorization: Bearer ${GITLAB_PUSH_TOKEN}"
  --form "source_branch=${update_branch}" --form "target_branch=master"
  --form "title=Auto-update Go dependencies"
  --form "description=Made by GitLab CI."
)
for assignee in $ASSIGNEES; do
  assignee_id="$(curl -H "Authorization: Bearer $GITLAB_PUSH_TOKEN" "https://gitlab.com/api/v4/users?username=$assignee" | jq '.[0].id' -r)"
  curl_flags+=(--form "assignee_ids[]=$assignee_id")
done

curl "${curl_flags[@]}" "https://gitlab.com/api/v4/projects/21078483/merge_requests"

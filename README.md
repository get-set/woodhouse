# woodhouse

A monolithic project for GitLab infrastructure team tools. Slack apps, CLI
subcommands, and other assorted things.

[[_TOC_]]

## Functionality

- Slack apps
  - Incident management (`/woodhouse incident`)
  - On-call handover (coming soon)
- Cronjobs (CI-scheduled commands)
  - Archive old incident Slack channels
  - Alertmanager silence issue updates (Helicopter) (coming soon)
  - On-call report generator (coming soon)
- Commands
  - notify-mirrored-mr

## Configuration

<!--
Please keep these tables aligned.
Example, if using vim: https://github.com/junegunn/vim-easy-align
-->

### Global flags

| Flag            | Environment variable   | Description                                                                     | Required | Default |
| ------          | :--------------------: | :-----------:                                                                   | :----:   | -----:  |
| log-format      | LOG_FORMAT             | Format in which to print structured logs. Valid values: json, logfmt (default). | n        | logfmt  |
| pushgateway-url | PUSHGATEWAY_URL        | Prometheus pushgateway to push metrics to from cronjobs.                        | n        |         |

### `serve` subcommand

Start HTTP server, for Slack apps and other HTTP things.

| Flag                               | Environment variable               | Description                                                                                                                            | Required | Default                   |
| ------                             | :--------------------:             | :-----------:                                                                                                                          | :----:   | -----:                    |
| listen-address                     | LISTEN_ADDRESS                     | Address to listen on.                                                                                                                  | n        | :8080                     |
| metrics-listen-address             | METRICS_LISTEN_ADDRESS             | Address to listen on to provide Prometheus metrics.                                                                                    | n        | :8081                     |
| slack-signing-secret               | SLACK_SIGNING_SECRET               | Signing secret used to validate that requests originate from Slack.                                                                    | y        |                           |
| slack-bot-access-token             | SLACK_BOT_ACCESS_TOKEN             | Access token for the app's bot user to access the Slack API.                                                                           | y        |                           |
| http-request-body-max-size-bytes   | HTTP_REQUEST_BODY_MAX_SIZE_BYTES   | Maximum HTTP request body size in bytes.                                                                                               | n        | 1048576                   |
| max-requests-per-second-per-ip     | MAX_REQUESTS_PER_SECOND_PER_IP     | Request rate limit per IP.                                                                                                             | n        | 10                        |
| gitlab-api-token                   | GITLAB_API_TOKEN                   | GitLab API token to use for creating issues.                                                                                           | y        |                           |
| gitlab-api-base-url                | GITLAB_API_BASE_URL                | GitLab API to use for creating issues.                                                                                                 | n        | https://gitlab.com/api/v4 |
| gitlab-gl-infra-group              | GITLAB_GL_INFRA_GROUP              | GitLab group for finding all SREs by membership.                                                                                       | n        | gitlab-com/gl-infra       |
| gitlab-issue-type                  | GITLAB_ISSUE_TYPE                  | Issue types for opening incidents, valid values are "incident" or "issue"                                                              | n        | incident                  |
| gitlab-production-project-path     | GITLAB_PRODUCTION_PROJECT_PATH     | GitLab project path for incident and change issues.                                                                                    | y        |                           |
| incident-channel-name-prefix       | INCIDENT_CHANNEL_NAME_PREFIX       | Name prefix of incident channels. Defaults to project name.                                                                            | n        |                           |
| incident-call-url                  | INCIDENT_CALL_URL                  | URL to a call to use for incidents. This is a secret, unless you like being zoombombed!                                                | y        |                           |
| gitlab-webhook-token               | GITLAB_WEBHOOK_TOKEN               | GitLab webhook secret token, to validate requests.                                                                                     | y        |                           |
| global-slash-command               | GLOBAL_SLASH_COMMAND               | Name of the global slash command.                                                                                                      | n        | woodhouse                 |
| incident-slash-command             | INCIDENT_SLASH_COMMAND             | Name of the incident slash command.                                                                                                    | n        | incident                  |
| change-slash-command               | CHANGE_SLASH_COMMAND               | Name of the change   slash command.                                                                                                    | n        | change                    |
| async-job-timeout-seconds          | ASYNC_JOB_TIMEOUT_SECONDS          | Timeout limit for async jobs.                                                                                                          | y        | 120                       |
| incident-slack-channel             | INCIDENT_SLACK_CHANNEL             | Slack channel in which to post incident updates. Obtain the ID by copying a link to the channel, and taking the last part of the path. | y        |                           |
| change-slack-channel               | CHANGE_SLACK_CHANNEL               | Slack channel in which to post newly created change management issues. Obtain the ID by copying a link to the channel, and taking the last part of the path. | y        |                           |
| gitlab-profile-slack-field-id      | GITLAB_PROFILE_SLACK_FIELD_ID      | Slack custom field ID for users' GitLab profile links.                                                                                 | n        | Xf494JUKE0                |
| pagerduty-integration-key-eoc      | PAGERDUTY_INTEGRATION_KEY_EOC      | Pagerduty events V2 API integration key for the Production service (pages the EOC).                                                    | n        |                           |
| pagerduty-integration-key-imoc     | PAGERDUTY_INTEGRATION_KEY_IMOC     | Pagerduty events V2 API integration key for the IMOC service.                                                                          | n        |                           |
| pagerduty-integration-key-cmoc     | PAGERDUTY_INTEGRATION_KEY_CMOC     | Pagerduty events V2 API integration key for the CMOC service.                                                                          | n        |                           |
| pagerduty-api-token                | PAGERDUTY_API_TOKEN                | Pagerduty API token.                                                                                                                   | y        |                           |
| pagerduty-escalation-policy-eoc    | PAGERDUTY_ESCALATION_POLICY_EOC    | Escalation policy ID for the EOC.                                                                                                      | n        |                           |
| pagerduty-escalation-policy-imoc   | PAGERDUTY_ESCALATION_POLICY_IMOC   | Escalation policy ID for the IMOC.                                                                                                     | n        |                           |
| http-request-max-duration-seconds  | HTTP_REQUEST_MAX_DURATION_SECONDS  | Maximum HTTP request duration in seconds.                                                                                              | n        | 30                        |
| shutdown-sleep-seconds             | SHUTDOWN_SLEEP_SECONDS             | Time to sleep on receipt of a signal before waiting for all requests to complete, then shutting down.                                  | n        | 0                         |

### `slack archive-incident-channels` subcommand

Archive old incident channels.

| Flag         | Environment variable         | Description                                                  | Required | Default |
| ------       | :--------------------:       | :-----------:                                                | :----:   | -----:  |
| access-token | SLACK_BOT_ACCESS_TOKEN       | Access token for the app's bot user to access the Slack API. | y        |         |
| gitlab-api-token | GITLAB_API_TOKEN       | GitLab API token to use for fetching issues. | y        |         |
| gitlab-api-base-url | GITLAB_API_BASE_URL       | GitLab API to use for fetching issues. | y        | https://gitlab.com/api/v4 |
| gitlab-production-project-path | GITLAB_PRODUCTION_PROJECT_PATH       | GitLab project path for incident and change issues. | y        |         |
| max-age-days | INCIDENT_CHANNEL_MAX_AGE     | Archive incident slack channels older than this many days    | n        | 14      |
| name-prefix  | INCIDENT_CHANNEL_NAME_PREFIX | Name prefix for incident slack channels.                     | y        |         |
| dry-run      | DRY_RUN                      | Don't actually archive any channels.                         | n        | false   |

### `gitlab notify-mirrored-mr` subcommand

When run inside a mirror's CI job, writes a note on any corresponding merge requests on the mirror source.

| Flag                | Environment variable          | Description                                                                | Required | Default                   |
| ------              | :--------------------:        | :-----------:                                                              | :----:   | -----:                    |
| gitlab-api-token    | GITLAB_API_TOKEN              | GitLab API token to use for creating notes.                                | y        |                           |
| gitlab-api-base-url | GITLAB_API_BASE_URL           | GitLab API to use for creating issues.                                     | n        | https://gitlab.com/api/v4 |
| project-path        | GITLAB_MIRROR_PATH            | Project path of mirror source, if different from the mirror target's path. | n        |                           |
| pipeline-status     | GITLAB_MIRROR_PIPELINE_STATUS | Status of the mirror's pipeline.                                           | n        | starting                  |
| dry-run             | DRY_RUN                       | Don't actually make any comments.                                          | n        | false                     |

### `gitlab follow-remote-pipeline` subcommand

When run inside a GitLab CI job, blocks on pipelines for the same commit running
on a mirror target/source.

| Flag                | Environment variable   | Description                                      | Required | Default                   |
| ------              | :--------------------: | :-----------:                                    | :----:   | -----:                    |
| gitlab-api-token    | GITLAB_API_TOKEN       | GitLab API token to use for following pipelines. | y        |                           |
| gitlab-api-base-url | GITLAB_API_BASE_URL    | GitLab API to use for following pipelines.       | n        | https://gitlab.com/api/v4 |

### `alertmanager notify-expiring-silences` subcommand

Find silences that are expiring soon and notify the on-call via slack.

| Flag                           | Environment variable           | Description                                                  | Required | Default                   |
| ------                         | :--------------------:         | :-----------:                                                | :----:   | -----:                    |
| access-token                   | SLACK_BOT_ACCESS_TOKEN         | Access token for the app's bot user to access the Slack API. | y        |                           |
| slack-channel                  | ALERTMANATER_SLACK_CHANNEL     | Slack channel in which to post expiring alert warnings.      | y        |                           |
| alertmanager-base-url          | ALERTMANAGER_BASE_URL          | Alertmanager API base URL.                                   | y        | http://localhost:9093     |
| alertmanager-external-base-url | ALERTMANAGER_EXTERNAL_BASE_URL | Alertmanager base URL, used to link to silences.             | n        | https://alerts.gitlab.net |
| silence-end-cutoff             | SILENCE_END_CUTOFF             | Cutoff for when silences expire.                             | y        | 8h                        |
| dry-run                        | DRY_RUN                        | Don't actually post to slack.                                | n        |                           |

## Developing

Build woodhouse:

```
make
```

You can use [ngrok](https://ngrok.com/) to expose a locally-running web server
on the public internet, e.g. to test Slack integration in a test workspace. This
won't always be necessary: see below for instructions on how to make a staging
deploy from a branch.

```
ngrok http --region <region> 8080
```

See the available configuration in the tables above, or run `./woodhouse
--help`, Or look at the flags in [`config.go`](cmd/woodhouse/config.go).

Follow the "Installing Slack App" instructions below to install a Slack app on a
Slack workspace you control. When configuring the callback URLs, use the ngrok
URL given to you by `ngrok http` as a base.

We might eventually create a staging deployment to test out branch builds on the
GitLab slack, but at the moment this is the only way to test changes to
Woodhouse.

You can use [`direnv`](https://direnv.net/) to automate the sourcing of a
`.envrc` file if you want.

Start woodhouse:

```
./woodhouse serve
```

You can now run Slack slash commands like `/woodhouse incident declare`, or
`/woodhouse echo`.

### Testing integrations

Woodhouse is an integration-heavy codebase, and as such it can be difficult to
gain confidence in changes to these integrations from unit testing alone. We
rely on manual testing of some integrations using Woodhouse's shared staging
environment.

1. Check in the `#woodhouse-staging` Slack channel to see if anyone else has
   borrowed staging without returning it.
1. If you suspect that someone forgot to announce they were done with staging,
   message them.
1. Announce that you are borrowing staging,
1. Create your branch and merge request.
1. Trigger the manual docker_image step on the gitlab.com CI pipeline for your
   MR. Wait for the Docker image to be built and pushed for your commit SHA.
1. If your MR introduces any new mandatory configuration parameters, add them to
   staging's tanka configuration, or if they are secret, to the Kubernetes
   Secret in staging. Instructions:
   https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/tree/master/environments/woodhouse
1. Trigger the manual deploy_staging step on the ops.gitlab.net CI pipeline for
   your MR. This will trigger a deploy to Woodhouse's staging environment in
   https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/tanka-deployments.
1. Try out your new feature.
1. Announce that you're done with staging in the Slack channel.
1. When your merge request is merged, this will trigger an automatic deployment
   to production.

## Operating

### Deployment and configuration

We (GitLab) deploy Woodhouse to our Kubernetes clusters using
[tanka](https://tanka.dev). Our deployment config is found
[here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/tree/master/environments/woodhouse).

Non-secret config is found in each tanka environment's `main.jsonnet`, while
secret config is manually managed in a Kubernetes Secret named `woodhouse`, in
a namespace of the same name.

We auto-deploy master to our "ops" cluster, and can make staging deploys from
branches (see above).

See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the source of truth on how tanka
deploys are triggered from this repository.

### Installing Slack App

1. Visit https://api.slack.com/apps.
1. "Create New App". Give it a name and a development workspace.
1. If Slack offers to create a bot user for you, do it. Otherwise make sure you
   configure a bot user later.
1. Read and understand the section below on slash commands, and plan what you
   will call the various commands based on whether or not this deployment is
   production or staging.
1. Configure the global slash command, `/woodhouse`.
   1. Set the request URL to wherever you will deploy Woodhouse, with a path of
      `/slack/slash`.
   1. Usage hint: `help|incident`
1. Configure the incident and change slash commands, `/incident` and `/change`.
   1. You can create the non-namespaced slash commands by adding additional slash
      commands to the existing woodhouse app under "Slash Commands" / "Create New Command".
   1. Use the same request URL, set to wherever you will deploy Woodhouse, with a path of
      `/slack/slash`.
   1. Usage hint: `<cmd>|help`. Ex: `incident|help`
1. Enable interactivity (in the features sidebar).
   1. Request URL: `<woodhouse domain>/slack/interactivity`
1. Navigate to the OAuth & Permissions page.
   1. Grab the bot user OAuth token. You'll need to feed this to Woodhouse.
   1. Under bot token scopes, add:
      - `chat:write`
      - `channels:manage`
      - `channels:read`
      - `chat:write.customize`
      - `users.profile:read`
1. Navigate to the Basic Information tab.
   1. Grab the signing secret for this app. You'll need to feed this to
      Woodhouse.
1. `@woodhouse` (or `@woodhouse-staging`, or whatever this particular instance
   of the bot user is called) in any channel you intend to use Woodhouse from.

#### A note about slash commands

Slash commands are all globally namespaced per Slack workspace, and Slack will
route slash commands to the most recently installed app that declares that
command.

Woodhouse supports 2 forms of Slack slash commands: namespaced and
non-namespaced. For example: `/woodhouse incident declare` vs `/incident
declare`.

The "/woodhouse" global command is configurable. It has to be, otherwise we
wouldn't be able to install a staging deployment of Woodhouse to the same slack
workspace as production.

All slash commands (except `echo`) have configurable names too, which affect
both the non-namespaced (`/name`) and namespaced (`/woodhouse name`) versions.
These names are configurable for 2 reasons:

- Support staging and production versions of non-namespaced commands in the same
  Slack workspace.
- Allow testing of command replacements without immediately replacing the
  non-namespaced command in Slack.

### GitLab webhook integration

Woodhouse can receive webhooks from GitLab. Currently, Woodhouse can receive
webhooks on its `/gitlab/incident-issue` endpoint as part of its incident
management functionality, and there may be more use cases for this in the
future.

1. Configure a webhook to Woodhouse's domain and endpoint in a GitLab project's
   hooks section, e.g.
   https://gitlab.com/gitlab-com/gl-infra/woodhouse-integration-test/hooks. Only
   check the boxes relevant to events Woodhouse can handle - currently issue
   events and confidential issue events.
1. Generate and configure a webhook secret, e.g. `pwgen -s 40 1`.
1. Add this webhook secret to Woodhouse's secret
   [config](cmd/woodhouse/config.go).

### Metrics

We collect metrics using Prometheus. All of them:
[here](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1h&g0.max_source_resolution=0s&g0.expr=%7Bjob%3D%22woodhouse%22%7D&g0.tab=1).

### Logs / Events

Woodhouse is sparse with logging, and in general emits one structured message
per event, where an event is an HTTP request or async job.

You can view our production Woodhouse logs in
[stackdriver](https://console.cloud.google.com/logs/viewer?project=gitlab-ops&minLogLevel=0&expandAll=false&timestamp=2020-10-14T08:29:26.315000000Z&customFacets=&limitCustomFacetWidth=true&dateRangeEnd=2020-10-14T08:29:26.566Z&interval=P1D&resource=k8s_container%2Fcluster_name%2Fops-gitlab-gke%2Fnamespace_name%2Fwoodhouse%2Fcontainer_name%2Fwoodhouse&dateRangeStart=2020-10-13T08:29:26.566Z&scrollTimestamp=2020-10-13T15:24:32.904889638Z).

## Maintainers

- [Igor W](https://gitlab.com/igorwwwwwwwwwwwwwwwwwwww)
- [Jarv](https://gitlab.com/jarv)

## Why put unrelated things into the same codebase / executable?

The main benefits are:

- One language (Go). This benefit is a double-edged sword, but:
  - Go is a language many on the GitLab infrastructure team have at least some
    familiarity with.
  - Code can be shared between the different components easily.
  - We're unlikely to make frequent changes to many of these components, and if
    some were written in different languages, the mean time between working in
    that language might be quite long, making it difficult to quickly jump in
    and make a change.
- New components don't have to reinvent the "project core" wheel: event logging,
  metrics, HTTP middleware (e.g. Slack authorization). If the codebase already
  contains a Slack app, or a CLI subcommand, slotting in new functionality
  should be simple.
- No need to set up build, test, and deployment pipelines for many small tools.
- One place to keep dependencies up to date.

The main drawbacks are:

- When making a change to a component, you and/or a CI job must build/index
  code, and run tests, for code you're not using.
- The built artifacts are larger than they would be for any one component
  extracted into its own codebase.

These drawbacks are most noticeable at large scale, which this project is
unlikely to reach.

Most of the benefits could be realized in a monorepo codebase that builds
several binaries, but the build and deployment pipelines are maximally simple
with the monolithic binary approach.

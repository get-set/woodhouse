package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	gogitlab "github.com/xanzy/go-gitlab"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	logger log.Logger
)

func main() {
	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))

	logger = createLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = level.NewInjector(logger, level.InfoValue())

	switch cmd {
	case serveCmd.FullCommand():
		fatal(serve())
	case slackArchiveIncidentChannelsCmd.FullCommand():
		fatal(archiveIncidentChannels())
	case gitlabNotifyMirroredMrCmd.FullCommand():
		fatal(notifyMirroredMr())
	case gitlabFollowRemotePipelineCmd.FullCommand():
		fatal(followRemotePipeline())
	case gitlabListPrivateEmails.FullCommand():
		fatal(listPrivateEmails())
	case alertmanagerNotifyExpiringSilencesCmd.FullCommand():
		fatal(notifyExpiringSilences())
	case handoverCmd.FullCommand():
		fatal(handoverGenerateIssue())
	case versionCmd.FullCommand():
		fatal(printVersion())
	default:
		fatal(fmt.Errorf("cmd %s unrecognized", cmd))
	}
}

func newGitLabClient(token, baseURL string, httpClient *http.Client) *gogitlab.Client {
	client, err := gogitlab.NewClient(
		token, gogitlab.WithBaseURL(baseURL),
		gogitlab.WithoutRetries(), // Use external retries, for better observability into how many failures occur
		gogitlab.WithHTTPClient(httpClient),
	)
	fatal(err)
	return client
}

func newHTTPClient() *http.Client {
	return &http.Client{Timeout: time.Second * 30}
}

func createLogger(w io.Writer) log.Logger {
	switch *logFormat {
	case "logfmt":
		return log.NewLogfmtLogger(w)
	case "json":
		return log.NewJSONLogger(w)
	default:
		// We can't call fatal without a logger!
		fmt.Printf("unsupported log format: %s\n", *logFormat)
		os.Exit(1)
		return nil
	}
}

func fatal(err error) {
	if err != nil {
		level.Error(logger).Log("event", "shutdown", "error", err)
		os.Exit(1)
	}
}

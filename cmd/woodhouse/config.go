package main

import "gopkg.in/alecthomas/kingpin.v2"

var (
	//////////////////////////////////////////////////////////////////////
	// Config
	//
	// Are you about to change any of this? Remember to update README.md!
	//////////////////////////////////////////////////////////////////////

	app = kingpin.New("woodhouse", "Assorted SRE tools.")

	logFormat = app.Flag("log-format", "Format in which to print structured logs. Valid values: json, logfmt (default).").
			Envar("LOG_FORMAT").Default("logfmt").String()
	pushGatewayURL = app.Flag("pushgateway-url", "Prometheus pushgateway to push metrics to from cronjobs.").
			Envar("PUSHGATEWAY_URL").Default("").String()

	serveCmd   = app.Command("serve", "Start HTTP server, for Slack apps and other HTTP things.")
	listenAddr = serveCmd.Flag("listen-address", "Address to listen on.").
			Short('l').Envar("LISTEN_ADDRESS").Default(":8080").String()
	metricsListenAddr = serveCmd.Flag("metrics-listen-address", "Address to listen on to provide Prometheus metrics.").
				Envar("METRICS_LISTEN_ADDRESS").Default(":8081").String()
	slackSigningSecret = serveCmd.Flag("slack-signing-secret", "Signing secret used to validate that requests originate from Slack.").
				Envar("SLACK_SIGNING_SECRET").Required().String()
	slackBotAccessToken = serveCmd.Flag("slack-bot-access-token", "Access token for the app's bot user to access the Slack API.").
				Envar("SLACK_BOT_ACCESS_TOKEN").Required().String()
	reqBodyMaxSize = serveCmd.Flag("http-request-body-max-size-bytes", "Maximum HTTP request body size in bytes.").
			Envar("HTTP_REQUEST_BODY_MAX_SIZE_BYTES").Default("1048576").Int64()
	maxReqsPerSecond = serveCmd.Flag("max-requests-per-second-per-ip", "Request rate limit per IP.").
				Envar("MAX_REQUESTS_PER_SECOND_PER_IP").Default("10").Int()
	gitlabAPIToken = serveCmd.Flag("gitlab-api-token", "GitLab API token to use for creating issues.").
			Envar("GITLAB_API_TOKEN").Required().String()
	gitlabAPIBaseURL = serveCmd.Flag("gitlab-api-base-url", "GitLab API to use for creating issues.").
				Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabIssueType = serveCmd.Flag("gitlab-issue-type", "Issue type for incident issues, defaults to type=incident, valid values are 'issue' or 'incident'.").
			Envar("GITLAB_ISSUE_TYPE").Default(issueTypeIncident).String()
	gitlabProductionProjectPath = serveCmd.Flag("gitlab-production-project-path", "GitLab project path for incident and change issues.").
					Envar("GITLAB_PRODUCTION_PROJECT_PATH").Required().String()
	gitlabGlInfraGroup = serveCmd.Flag("gitlab-gl-infra-group", "GitLab group for finding all SREs by membership.").
				Envar("GITLAB_GL_INFRA_GROUP").Default("gitlab-com/gl-infra").String()
	gitlabOpsCheckTriggerToken = serveCmd.Flag("gitlab-ops-check-trigger-token", "GitLab Ops trigger token to for triggering production checks.").
					Envar("GITLAB_OPS_CHECK_TRIGGER_TOKEN").Default("").String()
	gitlabOpsAPIBaseURL = serveCmd.Flag("gitlab-ops-api-base-url", "GitLab Ops API to use for triggering production checks.").
				Envar("GITLAB_OPS_API_BASE_URL").Default("https://ops.gitlab.net/api/v4").String()
	gitlabOpsCheckProject = serveCmd.Flag("gitlab-check-project", "Project on ops.gitlab.net to use for the production check trigger that adds a note to all incidents and change issues.").
				Envar("GITLAB_OPS_CHECK_PROJECT").Default("gitlab-org/release/tools").String()
	incidentChannelNamePrefix = serveCmd.Flag("incident-channel-name-prefix", "Name prefix of incident channels. Defaults to project name.").
					Envar("INCIDENT_CHANNEL_NAME_PREFIX").Default("").String()
	incidentCallURL = serveCmd.Flag("incident-call-url", "URL to a call to use for incidents. This is a secret, unless you like being zoombombed!").
			Envar("INCIDENT_CALL_URL").Required().String()
	gitlabWebhookToken = serveCmd.Flag("gitlab-webhook-token", "GitLab webhook secret token, to validate requests.").
				Envar("GITLAB_WEBHOOK_TOKEN").Required().String()
	globalSlashCommand = serveCmd.Flag("global-slash-command", "Name of the global slash command.").
				Envar("GLOBAL_SLASH_COMMAND").Default("woodhouse").String()
	incidentSlashCommand = serveCmd.Flag("incident-slash-command", "Name of the incident slash command.").
				Envar("INCIDENT_SLASH_COMMAND").Default("incident").String()
	changeSlashCommand = serveCmd.Flag("change-slash-command", "Name of the change slash command.").
				Envar("CHANGE_SLASH_COMMAND").Default("change").String()
	asyncJobTimeoutSeconds = serveCmd.Flag("async-job-timeout-seconds", "Timeout limit for async jobs.").
				Envar("ASYNC_JOB_TIMEOUT_SECONDS").Default("120").Int()

	// We could just inject the name, as the Slack API's sendMessage method
	// accepts channel names. However, this prevents us from linking to the
	// channel in replies to the invoking user. You need a channel ID for this,
	// and incredibly, the only way to get it is to list all conversations and
	// paginate through the results. This is too much hassle to be worth it, for a
	// parameter we'll rarely configure.
	incidentSlackChannelID = serveCmd.Flag("incident-slack-channel", "Slack channel in which to post incident updates. Obtain the ID by copying a link to the channel, and taking the last part of the path.").
				Envar("INCIDENT_SLACK_CHANNEL").Required().String()

	// This is the channel ID where we post newly created change requests, e.g.
	// #production.
	changeSlackChannelID = serveCmd.Flag("change-slack-channel", "Slack channel in which to post newly created change management issues. Obtain the ID by copying a link to the channel, and taking the last part of the path.").
				Envar("CHANGE_SLACK_CHANNEL").Required().String()

	// Slack custom fields are workspace-scoped: all users that populate the field
	// will have this key in their profile's custom fields obtained from
	// https://api.slack.com/methods/users.profile.get.
	//
	// We inject the ID rather than the more human-readable field label due to the
	// API parameter that returns these labels not being recommended for use by
	// Slack, and carrying a dire warning about heavy rate-limiting.
	//
	// The default value is the current GitLab Slack workspace field ID for GitLab
	// profiles.
	gitlabProfileSlackFieldID = serveCmd.Flag("gitlab-profile-slack-field-id", "Slack custom field ID for users' GitLab profile links.").
					Envar("GITLAB_PROFILE_SLACK_FIELD_ID").Default("Xf494JUKE0").String()

	// Set these to enable real pages to be generated when declaring incidents.
	pagerdutyIntegrationKeyEOC = serveCmd.Flag("pagerduty-integration-key-eoc", "Pagerduty events V2 API integration key for the Production service (pages the EOC).").
					Envar("PAGERDUTY_INTEGRATION_KEY_EOC").Default("").String()
	pagerdutyIntegrationKeyIMOC = serveCmd.Flag("pagerduty-integration-key-imoc", "Pagerduty events V2 API integration key for the IMOC service.").
					Envar("PAGERDUTY_INTEGRATION_KEY_IMOC").Default("").String()
	pagerdutyIntegrationKeyCMOC = serveCmd.Flag("pagerduty-integration-key-cmoc", "Pagerduty events V2 API integration key for the CMOC service.").
					Envar("PAGERDUTY_INTEGRATION_KEY_CMOC").Default("").String()
	pagerdutyAPIToken = serveCmd.Flag("pagerduty-api-token", "Pagerduty API token.").
				Envar("PAGERDUTY_API_TOKEN").Required().String()
	pagerdutyEscalationPolicyEOC = serveCmd.Flag("pagerduty-escalation-policy-eoc", "Escalation policy ID for the EOC.").
					Envar("PAGERDUTY_ESCALATION_POLICY_EOC").Default("").String()
	pagerdutyEscalationPolicyIMOC = serveCmd.Flag("pagerduty-escalation-policy-imoc", "Escalation policy ID for the IMOC.").
					Envar("PAGERDUTY_ESCALATION_POLICY_IMOC").Default("").String()

	// This timeout is a bit of a lie: it's configured for the server's read and
	// write timeouts, and also set on the request context. In practice, requests
	// might actually be able to live for up to double this time.
	reqMaxDuration = serveCmd.Flag("http-request-max-duration-seconds", "Maximum HTTP request duration in seconds.").
			Envar("HTTP_REQUEST_MAX_DURATION_SECONDS").Default("30").Int64()

	// Useful for kubernetes. When a pod is terminated, it is asynchronously
	// removed from the service load balancer. During that time, HTTP requests can
	// still be sent to the pod.
	shutdownSleepSeconds = serveCmd.Flag("shutdown-sleep-seconds", "Time to sleep on receipt of a signal before waiting for all requests to complete, then shutting down.").
				Envar("SHUTDOWN_SLEEP_SECONDS").Default("0").Int()

	slackCmd                           = app.Command("slack", "Perform tasks on Slack.")
	slackArchiveIncidentChannelsCmd    = slackCmd.Command("archive-incident-channels", "Archive old incident channels.")
	archiveIncidentChannelsAccessToken = slackArchiveIncidentChannelsCmd.Flag("access-token", "Access token for the app's bot user to access the Slack API.").
						Envar("SLACK_BOT_ACCESS_TOKEN").Required().String()
	archiveIncidentChannelsGitlabAPIToken = slackArchiveIncidentChannelsCmd.Flag("gitlab-api-token", "GitLab API token to use for fetching issues.").
						Envar("GITLAB_API_TOKEN").Required().String()
	archiveIncidentChannelsGitlabAPIBaseURL = slackArchiveIncidentChannelsCmd.Flag("gitlab-api-base-url", "GitLab API to use for fetching issues.").
						Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	archiveIncidentChannelsGitlabProductionProjectPath = slackArchiveIncidentChannelsCmd.Flag("gitlab-production-project-path", "GitLab project path for incident and change issues.").
								Envar("GITLAB_PRODUCTION_PROJECT_PATH").Required().String()
	archiveIncidentChannelsMaxAge = slackArchiveIncidentChannelsCmd.Flag("max-age-days", "Archive incident slack channels older than this many days").
					Envar("INCIDENT_CHANNEL_MAX_AGE").Default("14").Int()
	archiveIncidentChannelsPrefix = slackArchiveIncidentChannelsCmd.Flag("name-prefix", "Name prefix for incident slack channels.").
					Envar("INCIDENT_CHANNEL_NAME_PREFIX").Required().String()
	archiveIncidentChannelsDryRun = slackArchiveIncidentChannelsCmd.Flag("dry-run", "Don't actually archive any channels.").
					Envar("DRY_RUN").Default("false").Bool()

	gitlabCmd                   = app.Command("gitlab", "Perform tasks on GitLab.")
	gitlabNotifyMirroredMrCmd   = gitlabCmd.Command("notify-mirrored-mr", "When run inside a mirror's CI job, writes a note on any corresponding merge requests on the mirror source.")
	gitlabNotifyMirroredMrToken = gitlabNotifyMirroredMrCmd.Flag("gitlab-api-token", "GitLab API token to use for creating notes.").
					Envar("GITLAB_API_TOKEN").Required().String()
	gitlabNotifyMirroredMrBaseURL = gitlabNotifyMirroredMrCmd.Flag("gitlab-api-base-url", "GitLab API to use for creating issues.").
					Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabNotifyMirroredMrProjectPath = gitlabNotifyMirroredMrCmd.Flag("project-path", "Project path of mirror source, if different from the mirror target's path.").
						Envar("GITLAB_MIRROR_PATH").Default("").String()
	gitlabNotifyMirroredMrPipelineStatus = gitlabNotifyMirroredMrCmd.Flag("pipeline-status", "Status of the mirror's pipeline.").
						Envar("GITLAB_MIRROR_PIPELINE_STATUS").Default("starting").String()
	gitlabNotifyMirroredMrDryRun = gitlabNotifyMirroredMrCmd.Flag("dry-run", "Don't actually make any comments.").
					Envar("DRY_RUN").Default("false").Bool()

	gitlabFollowRemotePipelineCmd   = gitlabCmd.Command("follow-remote-pipeline", "When run inside a GitLab CI job, blocks on pipelines for the same commit running on a mirror target/source.")
	gitlabFollowRemotePipelineToken = gitlabFollowRemotePipelineCmd.Flag("gitlab-api-token", "GitLab API token to use for following pipelines.").
					Envar("GITLAB_API_TOKEN").Required().String()
	gitlabFollowRemotePipelineBaseURL = gitlabFollowRemotePipelineCmd.Flag("gitlab-api-base-url", "GitLab API to use for following pipelines.").
						Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()

	gitlabListPrivateEmails                  = gitlabCmd.Command("list-private-emails", "List on-call emails that are private, and thus will not be addressable by woodhouse tooling.")
	gitlabListPrivateEmailsPagerdutyAPIToken = gitlabListPrivateEmails.Flag("pagerduty-api-token", "Pagerduty API token.").
							Envar("PAGERDUTY_API_TOKEN").Required().String()
	gitlabListPrivateEmailsGitlabAPIBaseURL = gitlabListPrivateEmails.Flag("gitlab-api-base-url", "GitLab API base URL.").
						Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabListPrivateEmailsGitlabAPIToken = gitlabListPrivateEmails.Flag("gitlab-api-token", "GitLab API token.").
						Envar("GITLAB_API_TOKEN").Required().String()
	gitlabListPrivateEmailsPagerdutyEscalationPolicy = gitlabListPrivateEmails.Flag("pagerduty-escalation-policy", "Escalation policy IDs.").
								Envar("PAGERDUTY_ESCALATION_POLICY").Required().Strings()

	alertmanagerCmd                       = app.Command("alertmanager", "Perform tasks on Prometheus alertmanager.")
	alertmanagerNotifyExpiringSilencesCmd = alertmanagerCmd.Command("notify-expiring-silences", "Find silences that are expiring soon and notify the on-call via slack.")
	alertmanagerSlackAccessToken          = alertmanagerNotifyExpiringSilencesCmd.Flag("access-token", "Access token for the app's bot user to access the Slack API.").
						Envar("SLACK_BOT_ACCESS_TOKEN").Required().String()
	alertmanagerSlackChannelID = alertmanagerNotifyExpiringSilencesCmd.Flag("slack-channel", "Slack channel in which to post expiring alert warnings.").
					Envar("ALERTMANATER_SLACK_CHANNEL").Required().String()
	alertmanagerBaseURL = alertmanagerNotifyExpiringSilencesCmd.Flag("alertmanager-base-url", "Alertmanager API base URL.").
				Envar("ALERTMANAGER_BASE_URL").Default("http://localhost:9093").String()
	alertmanagerExternalBaseURL = alertmanagerNotifyExpiringSilencesCmd.Flag("alertmanager-external-base-url", "Alertmanager base URL, used to link to silences.").
					Envar("ALERTMANAGER_EXTERNAL_BASE_URL").Default("https://alerts.gitlab.net").String()
	alertmanagerNotifyExpiringSilencesDryRun = alertmanagerNotifyExpiringSilencesCmd.Flag("dry-run", "Don't actually post to slack.").
							Envar("DRY_RUN").Default("false").Bool()
	alertmanagerNotifyExpiringSilencesCutoff = alertmanagerNotifyExpiringSilencesCmd.Flag("silence-end-cutoff", "Cutoff for when silences expire.").
							Envar("SILENCE_END_CUTOFF").Default("8h").Duration()

	handoverCmd               = app.Command("handover", "Generate handover issue for SRE on-call.")
	handoverPagerdutyAPIToken = handoverCmd.Flag("pagerduty-api-token", "Pagerduty API token.").
					Envar("PAGERDUTY_API_TOKEN").Required().String()
	handoverGitlabAPIBaseURL = handoverCmd.Flag("gitlab-api-base-url", "GitLab API to use for creating issues.").
					Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	handoverGitlabAPIToken = handoverCmd.Flag("gitlab-api-token", "GitLab API token to use for creating issues.").
				Envar("GITLAB_API_TOKEN").Required().String()
	handoverGitlabProductionProjectPath = handoverCmd.Flag("gitlab-production-project-path", "GitLab project path for incident and change issues.").
						Envar("GITLAB_PRODUCTION_PROJECT_PATH").Required().String()
	handoverGitlabHandoverProjectPath = handoverCmd.Flag("gitlab-handover-project-path", "GitLab project path for handover issues.").
						Envar("GITLAB_HANDOVER_PROJECT_PATH").Required().String()
	handoverPagerdutyEscalationPolicyEOC = handoverCmd.Flag("pagerduty-escalation-policy-eoc", "Escalation policy ID for the EOC.").
						Envar("PAGERDUTY_ESCALATION_POLICY_EOC").Required().String()
	handoverPagerdutyEscalationPolicyIMOC = handoverCmd.Flag("pagerduty-escalation-policy-imoc", "Escalation policy ID for the IMOC.").
						Envar("PAGERDUTY_ESCALATION_POLICY_IMOC").Required().String()
	handoverPagerdutyServiceIDs = handoverCmd.Flag("pagerduty-service-ids", "Service IDs to consider for listing incidents.").
					Envar("PAGERDUTY_SERVICE_IDS").Required().Strings()
	handoverNow = handoverCmd.Flag("now", "Override current time to test handover generation at different times. Specify as RFC3339.").
			Default("").String()
	handoverDryRun = handoverCmd.Flag("dry-run", "Print handover to stdout instead of creating an issue.").
			Envar("DRY_RUN").Default("false").Bool()

	versionCmd = app.Command("version", "Print build information.")

	// Did you just change any config above this message? Remember to update
	// README.md!
)

const (
	issueTypeIncident string = "incident"
	issueTypeIssue    string = "issue"
)

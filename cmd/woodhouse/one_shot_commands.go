package main

import (
	"context"
	"fmt"
	"os"
	"sort"
	"time"

	"github.com/go-kit/kit/log/level"

	"github.com/xanzy/go-gitlab"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"

	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/handover"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/version"
	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/alertmanager"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

func archiveIncidentChannels() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()
	slackClient := slack.New(*archiveIncidentChannelsAccessToken, slack.OptionHTTPClient(httpClient))
	gitlabClient := newGitLabClient(*archiveIncidentChannelsGitlabAPIToken, *archiveIncidentChannelsGitlabAPIBaseURL, httpClient)

	monitor := woodhouse.NewJobMonitor(prometheus.NewRegistry(), *pushGatewayURL)
	run := func(ctx context.Context) error {
		return slackutil.ArchiveIncidentChannels(
			ctx, logger, slackClient, gitlabClient,
			*archiveIncidentChannelsGitlabProductionProjectPath,
			*archiveIncidentChannelsPrefix,
			time.Duration(*archiveIncidentChannelsMaxAge)*24*time.Hour,
			*archiveIncidentChannelsDryRun,
		)
	}
	return monitor.Run(ctx, "archive_incident_channels", time.Hour*25, run)
}

func notifyMirroredMr() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*2)
	defer cancel()
	client := newGitLabClient(*gitlabNotifyMirroredMrToken, *gitlabNotifyMirroredMrBaseURL, newHTTPClient())
	return gitlabutil.NotifyMergeRequestFromMirrorSource(ctx, client, logger, *gitlabNotifyMirroredMrProjectPath, *gitlabNotifyMirroredMrPipelineStatus, *gitlabNotifyMirroredMrDryRun)
}

func followRemotePipeline() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Hour)
	defer cancel()
	client := newGitLabClient(*gitlabFollowRemotePipelineToken, *gitlabFollowRemotePipelineBaseURL, newHTTPClient())

	// Assume that this will only be used on mirrors with identical project paths.
	// If this assumption doesn't hold, we can always make it configurable.
	return gitlabutil.FollowRemotePipeline(ctx, logger, client, os.Getenv("CI_PROJECT_PATH"), os.Getenv("CI_COMMIT_SHA"))
}

func listPrivateEmails() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()

	pdClient := gopagerduty.NewClient(*gitlabListPrivateEmailsPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	gitlabClient := newGitLabClient(*gitlabListPrivateEmailsGitlabAPIToken, *gitlabListPrivateEmailsGitlabAPIBaseURL, httpClient)

	privateEmails, err := gitlabutil.ListPrivateEmails(ctx, pdClient, gitlabClient, *gitlabListPrivateEmailsPagerdutyEscalationPolicy)
	if err != nil {
		return err
	}

	sort.Strings(privateEmails)

	for _, email := range privateEmails {
		fmt.Println(email)
	}

	return nil
}

func notifyExpiringSilences() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()
	slackClient := slack.New(*alertmanagerSlackAccessToken, slack.OptionHTTPClient(httpClient))

	alertmanagerClient := alertmanager.NewClient(httpClient, *alertmanagerBaseURL)

	silences, err := alertmanager.GetExpiringSilences(ctx, alertmanager.GetExpiringSilencesOptions{
		AlertmanagerClient: alertmanagerClient,
		Cutoff:             *alertmanagerNotifyExpiringSilencesCutoff,
		StartTime:          time.Now().UTC(),
	})

	if err != nil {
		return err
	}

	return slackutil.NotifyExpiringSilences(ctx, logger, slackutil.NotifyExpiringSilencesOptions{
		Silences:                    silences,
		SlackClient:                 slackClient,
		AlertmanagerExternalBaseURL: *alertmanagerExternalBaseURL,
		SlackChannelID:              *alertmanagerSlackChannelID,
		Cutoff:                      *alertmanagerNotifyExpiringSilencesCutoff,
		DryRun:                      *alertmanagerNotifyExpiringSilencesDryRun,
	})
}

func handoverGenerateIssue() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()

	pdClient := gopagerduty.NewClient(*handoverPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	pager := &pagerduty.Pager{HTTPClient: httpClient, PagerdutyClient: pdClient}
	gitlabClient := newGitLabClient(*handoverGitlabAPIToken, *handoverGitlabAPIBaseURL, httpClient)

	now := time.Now().UTC()
	if *handoverNow != "" {
		var err error
		now, err = time.Parse(time.RFC3339, *handoverNow)
		if err != nil {
			fatal(fmt.Errorf("could not parse --now as RFC3339 timestamp: %v", err))
		}
	}

	g := &handover.Generator{
		Pager:        pager,
		GitlabClient: gitlabClient,
		Logger:       logger,

		PagerdutyEscalationPolicyEOC:  *handoverPagerdutyEscalationPolicyEOC,
		PagerdutyEscalationPolicyIMOC: *handoverPagerdutyEscalationPolicyIMOC,
		PagerdutyServiceIDs:           *handoverPagerdutyServiceIDs,
		ProductionProjectPath:         *handoverGitlabProductionProjectPath,
	}
	body, title, err := g.Generate(ctx, now)
	if err != nil {
		return err
	}

	if *handoverDryRun {
		fmt.Println(body)
		return nil
	}

	issue, _, err := gitlabClient.Issues.CreateIssue(
		*handoverGitlabHandoverProjectPath,
		&gitlab.CreateIssueOptions{
			Title:       gitlab.String(title),
			Description: gitlab.String(body),
		},
	)
	if err != nil {
		return err
	}

	level.Warn(logger).Log("action", "handover", "msg", "created handover issue", "url", issue.WebURL)

	return nil
}

func printVersion() error {
	fmt.Println(version.Print("woodhouse"))
	return nil
}

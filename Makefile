.PHONY: build
LDFLAGS += -X github.com/prometheus/common/version.Revision=$(shell git rev-parse --short HEAD)
LDFLAGS += -X github.com/prometheus/common/version.Branch=$(shell git rev-parse --abbrev-ref HEAD)
LDFLAGS += -X github.com/prometheus/common/version.BuildDate=$(shell git log -1 --format=%cI)
build:
	CGO_ENABLED=0 go build -ldflags '$(LDFLAGS)' ./cmd/woodhouse

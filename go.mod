module gitlab.com/gitlab-com/gl-infra/woodhouse

go 1.16

require (
	github.com/PagerDuty/go-pagerduty v1.3.0
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/iancoleman/strcase v0.1.3
	github.com/oklog/run v1.1.0
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.20.0
	github.com/rs/xid v1.3.0
	github.com/sethvargo/go-retry v0.1.0
	github.com/slack-go/slack v0.8.2
	github.com/stretchr/testify v1.6.1
	github.com/xanzy/go-gitlab v0.50.1
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
